<%@page import="core.NorthApiCommunicator"%>
<%@page import="core.ResponseMessageDataTransfer"%>
<%@page import="core.RequestMessageDataTransfer"%>
<%@page import="core.DataClass"%>
<%@page import="java.lang.reflect.*"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="core.UtilityClass"%>


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<jsp:include page="/Servlet" flush="false">
	<jsp:param name="action" value="getSessionKeyFromJSP" />
</jsp:include>

<jsp:include page="/Servlet" flush="false">
	<jsp:param name="action" value="getVbsAddress" />
</jsp:include>



<%
	NorthApiCommunicator north = new NorthApiCommunicator(request.getSession()); 
    
    String sKey  = (String)session.getAttribute("sessionKey");
    
    System.out.println("Key is :" +sKey); 
    
    String vbsAdrr  = (String)session.getAttribute("vbsAddress");
    	    
    System.out.println("VBS ADDRESS is :" + vbsAdrr); 
  
    String manuName = (String)session.getAttribute("user");
    
   // north.setRequest(request);
    
   // north.setResponse(response);
    
   // UtilityClass.ftpDispacher();
    
    /*
    /    Get the info regarding the manufacturer .
    /    Message 3.4 . North API 1.5 .
    /    Get manufacturer name from Session .
    */
    
      //DFDnyfcTMxd6JulDeteDoS4OWxT
  /*  ResponseMessageDataTransfer responseObject = north.sendMessage(new RequestMessageDataTransfer("1.4","null","DFDnyfcTMxd6JulDeteDoS4OWxT","ManuInfo", new DataClass(){
   	 
  	  String name = "Ford";
  	  
  	 public String getName() {
  	  	return name;
  	  }

  	  public void setName(String name) {
  	  	this.name = name;
  	  }
  	  
   },"1"));   
    
      LinkedHashMap<String, String> dataArrayManuInfo = new LinkedHashMap<String, String>();
      
      dataArrayManuInfo = (LinkedHashMap<String,String>)responseObject.getData();
	  
      System.out.println("output:  " + dataArrayManuInfo.get("uuid")); 
      
      String manuUUid = dataArrayManuInfo.get("uuid");  */
    
   /*north.setRequest(request);
   /north.setResponse(response);
   /ResponseMessageDataTransfer resDto = north.sendMessage(new RequestMessageDataTransfer("1.4","null","null","Ping","null","1"));
   /System.out.println("output:  " + resDto.getType()); 
   */
%>


<!DOCTYPE html>
<html>
<head>

<!--   curl -i -H "Content-Type:application/json" -H "Accept: application/json" -X POST -d '{"protocol":"1.4","id":"null","session":"null","type":"Ping","data":"null","seq":1}' -v http://204.68.122.215:8080/app-proc -->

<link rel="stylesheet" type="text/css" href="styles/CarSyncStyles.css" />

<link rel="stylesheet" href="styles/jquery-ui.css" />

<script type="text/javascript"
	src="javascript/jquery/jquery-1.10.2.min.js">
	
</script>

<script type="text/javascript"
	src="javascript/jquery-ui-1.10.3/ui/jquery-ui.js">
	
</script>

<script type="text/javascript" src="javascript/jquery/uploadPlugin.js">
	
</script>

<script type="text/javascript" src="javascript/CarSyncJS.js"></script>

<script type="text/javascript">
	getSessionKey();

	//  The array for autofill.

	$(function() {
		$("#widgetOne").dialog({

			maxWidth : 600,
			maxHeight : 400,
			width : 600,
			height : 400,
			hide : "explode",
			dialogClass : 'newDialog',
			position : {
				my : 'left top',
				at : 'left top',
				of : $('#auxilDiv')
			}

		});
	});

	$(function() {
		$("#widgetTwo").dialog({
			position : [ 825, 255 ],
			maxWidth : 600,
			maxHeight : 400,
			width : 600,
			height : 400,
			hide : "explode",
			dialogClass : 'newDialog',
			position : {
				my : 'right top',
				at : 'right top',
				of : $('#auxilDiv')
			}

		});
	});

	$(function() {
		$("#widgetThree").dialog({
			position : [ 825, 255 ],
			maxWidth : 600,
			maxHeight : 400,
			width : 600,
			height : 400,
			hide : "explode",
			dialogClass : 'newDialog',
			position : {
				my : 'left top',
				at : 'center ',
				of : $('#auxilDiv')
			}

		});
	});

	$(function() {
		$("#packageManager").dialog({
			position : [ 825, 255 ],
			maxWidth : 600,
			maxHeight : 400,
			width : 600,
			height : 400,
			hide : "explode",
			dialogClass : 'newDialog',
			position : {
				my : 'right top',
				at : 'bottom ',
				of : $('#auxilDiv')
			}

		});
	});

	$(document).ready(
			function() {

				$("#addVehTab").dialog(

				{

					width : 600,
					height : 570,
					autoOpen : false,
					show : "slide",
					hide : "explode",
					dialogClass : 'newDialog',
				});

				$("#addRTab").dialog(

				{

					width : 600,
					height : 570,
					autoOpen : false,
					show : "slide",
					hide : "explode",
					dialogClass : 'newDialog',

				});

				$("#addNewUpdate").dialog(

				{

					width : 600,
					height : 570,
					autoOpen : false,
					show : "slide",
					hide : "explode",
					dialogClass : 'newDialog',

				});

				$("#addIcon").click(function() {

					$("#addVehTab").dialog("open");
					return false;

				}

				);

				$("#addCreateButton").click(function() {

					$("#addNewUpdate").dialog("open");
					return false;

				}

				);

				$("#searchRPButton").click(function() {

					$("#packageManager").dialog("open");
					return false;

				}

				);

				$("#createPackageButton").click(function() {

					$("#addRTab").dialog("open");
					return false;

				}

				);

				$("#uploadFile").click(
						function() {

							var fileName = $('#myFile').val();

							if (fileName.indexOf("\\") != -1) {

								var array = fileName.split("\\");
								var index = array.length - 1;
								fileName = array[index];

							}

							//http://204.68.122.215/package-upload/2df21282-f8c8-4d2d-a5c4-249a064d55f3-vfvfvfvfv/ 

							$("#myFile").upload(
									"./Servlet?action=fileUpload&fileName="
											+ fileName

									//$("#myFile").upload("http://204.68.122.215/package-upload/2df21282-f8c8-4d2d-a5c4-249a064d55f3-vfvfvfvfv/"
									, {

										myData : $('#myFile').val()

									}

									, function(success) {

										console.log(success);

									}, function(prog, value) {

										$("#prog").val(value);

									});

						}

				);

				$("#createPackageButton").hover(function() {
					$(this).fadeOut(100);
					$(this).fadeIn(500);
				});

				$(".closeButtons").hover(function() {
					$(this).fadeOut(100);
					$(this).fadeIn(500);
				});

			});
</script>

<!--  background-color: #F9A7AE; -->
<script type="text/css">

   .ui-dialog-titlebar {
  background-color: white;

border-style:solid;
border-top:thick #007abb;
border-bottom: 1px solid #007abb;
border-left:none;
border-right:none;
  background-image: none;
  color: #000;
  }

</script>

<title>Synchronization Portal</title>

</head>
<body>



	<%
		if(request.getSession().getAttribute("user") != null){
	%>


	<div id="widgetOne" title="Vehicle Status" class="widgets">


		<div id="addVehTab" title="Add new vehicle" class="widgets">
			<form>

				<table id="addVehTable">


					<tr>
						<td>
							<p id="">Manufacturer id :</p>
						</td>
						<td><input type="text" id="inputMan" class="addInput" /></td>
					</tr>

					<tr>
						<td>
							<p id="">Model :</p>
						</td>
						<td><input type="text" id="inputModel" class="addInput" /></td>
					</tr>

					<tr>
						<td>
							<p id="">Type :</p>
						<td><input type="text" id="inputType" class="addInput" /></td>
					</tr>

					<tr>
						<td>
							<p id="">Year :</p>
						<td><input type="text" id="inputYear" class="addInput" /></td>
					</tr>

					<tr>
						<td>
							<p id="">Date of modification :</p>
						<td><input type="text" id="inputDate" class="addInput" /></td>
					</tr>

					<tr>
						<td>
							<p id="">VIN :</p>
						<td><input type="text" id="inputVin" class="addInput" /></td>
					</tr>

					<tr>
						<td>
							<p id="">Group name :</p>
						<td><input type="text" id="inputGroup" class="addInput" /></td>
					</tr>

					<tr>
						<td>
							<p id="">Description :</p>
						<td><input type="text" id="inputDescription" class="addInput" />
						</td>
					</tr>
				</table>
			</form>

			<button id="addVehButton" onclick="addNewVehicle()"
				class="portalButtons">Add new vehicle</button>

		</div>

		<!-- Create new update file -->

		<div id="addNewUpdate" title="Create new update file" class="widgets">
			<form>

				<table id="createUfTable">

					<tr>
						<td>
							<p id="">Name :</p>
						</td>
						<td><input type="text" id="inputUFName" class="addInput" /></td>
					</tr>

					<tr>
						<td>
							<p id="">Description :</p>
						</td>
						<td><input type="text" id="inputUFDesc" class="addInput" /></td>
					</tr>

					<tr>
						<td>
							<p id="">Type :</p>
						<td><input type="text" id="inputUFType" class="addInput" /></td>
					</tr>

					<tr>
						<td>
							<p id="">Provider :</p>
						<td><input type="text" id="inputUFProvider" class="addInput" />
						</td>
					</tr>


				</table>
			</form>

			<button onclick="createNewUF()" class="portalButtons">
				Define new update file</button>

		</div>







		<div id="addRTab" title="Add new Package" class="widgets">
			<form>

				<table id="addRPTable">


					<tr>
						<td>
							<p id="">Name :</p>
						</td>
						<td><input type="text" id="packageName" class="addInput" /></td>
					</tr>


					<tr>
						<td>
							<p id="">Type :</p>
						<td><input type="text" id="packageType" class="addInput" /></td>
					</tr>





					<tr>
						<td>
							<p id="">RP Description :</p>
						<td><input type="text" id="packageDescription"
							class="addInput" /></td>
					</tr>
				</table>
			</form>

			<button id="createPackage" onclick="addNewPackage()"
				class="portalButtons">Create Package</button>

		</div>

		<input type="text" id="vinSearch" onkeyup="getVinsAutoFil(this.value)" />

		<div id="outPutForSearchVeh">

			<img src="img/01c.search.png" id="searchImage"
				onclick="validationFunction('vinSearch')  ; getVehicleInfo(document.getElementById('vinSearch').value)" />

			<table id="outPutTableManu">
				<tr>
					<td>Model :</td>
					<td id="modelOutput"></td>
				</tr>
				<tr>
					<td>VIN :</td>
					<td id="vinOutput"></td>
				</tr>
				<tr>
					<td>Type :</td>
					<td id="typeOutput"></td>
				</tr>
				<tr>
					<td>Year :</td>
					<td id="yearOutput"></td>
				</tr>
				<tr>
					<td>Dom :</td>
					<td id="domOutput"></td>
				</tr>
			</table>


			<!--   <p id = "pvin" class = "vehInfoClass">Model :</p> <div  id = "modelOutput"></div>
   <p id = "pmodel"  class = "vehInfoClass">VIN :</p>  <div  id = "vinOutput"></div>
   <p id = "ptype"  class = "vehInfoClass">Type :</p> <div  id = "typeOutput"></div>
   <p id = "pyear"  class = "vehInfoClass">Year :</p>  <div  id = "yearOutput"></div>
   <p id = "pdom"  class = "vehInfoClass">Dom :</p>  <div  id = "domOutput"></div>-->

			<br>

			<!--   <a href="#" id = "remBoat" onclick="removeBoat($('#vinOutput').html())"  class = "portalButtons">&nbsp;Remove vehicle&nbsp;</a>  -->

			<button id="remBoat" onclick="removeBoat($('#vinOutput').html())"
				class="portalButtons">&nbsp;Remove vehicle&nbsp;</button>

			<!-- <img src = "img/add.png"  id = "addIcon"   alt = "Add Vehicle" />  -->

		</div>


		<br>
		<hr id="hrOne">


		<button id="addIcon" class="portalButtons">Add new vehicle</button>

	</div>

	<%
		String str = "{\"vin\":\"vehicle VIN\"}";
	  System.out.println(str);
	  
	 
	  //String sessionKey  = 
	  
	  //Object objectData = new DataClass<String>("SAJAC18R1AMV01126"); 
	  //Field field = objectData.getClass().getField("field");
	  // System.out.println(field.getName());
	 
	 
	/*  DataClass objectData = new DataClass(){
		 
		  String vin = "ZBM9DW5OJ4PYREY3F";
		  
		  public String getVin() {
		  	return vin;
		  }

		  public void setVin(String vin) {
		  	this.vin = vin;
		  }
		 
	 };  */
	 
	 
	 
	 /*
	 /    Message 3.15
	 /    Group info by manufacturer .
	 /
	 */
	 
	 /*
	 /     example manu ID - ford cd307a9a-14e8-4e45-b474-1fc95d476f2d .
	 */
	 
	 
	 // ResponseMessageDataTransfer resDto2 = north.sendMessage(new RequestMessageDataTransfer("1.4","null","DFDnyfcTMxd6JulDeteDoS4OWxT","VehicleInfo",objectData,"1"));
	 
	 /*
	 /    Tutaj musi isc manu id wyciagniete po manu name .
	 /
	 */
	 

	  String json =  "{\"protocol\":\"1.6\",\"id\":\"1\",\"session\":\""+sKey+"\",\"type\":\"GroupInfoByManufacturer\",\"data\":{\"manu-id\":\"cd307a9a-14e8-4e45-b474-1fc95d476f2d\"},\"seq\":1}";
	  //cd307a9a-14e8-4e45-b474-1fc95d476f2d
	  
	  System.out.println(json);
			  
	  ResponseMessageDataTransfer resDtoGroups = north.sendMessageUsingJson(json);
	  
	  LinkedHashMap<String,ArrayList> groupDataArray = (LinkedHashMap<String,ArrayList>)resDtoGroups.getData();
	  

	  //if(groupDataArray.)
	  
	  
	  ArrayList<LinkedHashMap> countList =  groupDataArray.get("groups");
	  
	  System.out.println("vehicles count:  " +  countList.get(0).get("vehicles count") );
	 
	 
	  
	 /*
	 /      Co sie stanie jak status jest 1 .
	 /
	 */
	%>

	<div id="widgetTwo" title="Account Summary" class="widgets">

		<p id="packageGroups">Company name</p>
		<div id="packageGroupsDisplay"><%=manuName%></div>

		<p id="totalAuto">Number of Vehicles by Group</p>
		<div id="totalAutoDisplay">

			<table id="groupsTable">

				<tr>
					<td>Group</td>
					<td class="classTable" id="group">No of vehicles</td>
				</tr>
				<tr>
					<td>QNX</td>
					<td class="classTable" id="qnxTable"><%=countList.get(0).get("vehicles count")%>
					</td>
				</tr>
				<tr>
					<td>TIZEN</td>
					<td class="classTable" id="tizenTable"><%=countList.get(0).get("vehicles count")%>
					</td>
				</tr>

			</table>

		</div>
		<p id="updPand">Updates pending</p>
		<div id="updPandDisplay"></div>
		<p id="uptApp">Updates applied</p>
		<div id="uptAppDisplay"></div>
		<p id="packageGroups">Packages groups</p>
		<div id="packageGroupsDisplay"></div>

	</div>


	<div id="widgetThree" title="Updates Manager" class="widgets">


		<script type="text/javascript">
			var uuid = "";
		</script>

		<!-- <a href = "#"  id = "createUpdateFile" >&nbsp;Create new update file&nbsp;</a> -->
		<button id="addCreateButton" class="portalButtons">Create new
			update file</button>
		<br>
		<hr>
		<br> <input type="text" id="searchUF" class="addInput"
			onkeyup="getUFAutoFil(this.value)" /> <br>
			
						<img src="img/01c.search.png" id="searchImage2"
				onclick="validationFunction('searchUF')  ; getUFinfo()" />



           <!--  
		<button id="searchUFButton"
			onclick="validationFunction('searchUF')  ;getUFinfo()"
			class="portalButtons">Find an Update File</button>
           -->



		<table id="ufDataTable">
			<tr>
				<td>description</td>
				<td id="desc" class="ufDataClass"></td>
			</tr>
			<tr>
				<td>name</td>
				<td id="name" class="ufDataClass"></td>
			</tr>
			<tr>
				<td>type</td>
				<td id="type" class="ufDataClass"></td>
			</tr>
			<tr>
				<td>provider</td>
				<td id="prov" class="ufDataClass"></td>
			</tr>
			<tr>
				<td>versions</td>
				<td id="versi" class="ufDataClass"></td>
			</tr>

		</table>


		<!--  "data":{"uuid":"2df21282-f8c8-4d2d-a5c4-249a064d55f3","description":null,"name":4,"type":"type","provider":"provider","versions":[["4.0.0","4.1.0"]]}-->

		<button id="addNewVersionToUFFile" onclick="showUfVersionTab(uuid)"
			class="portalButtons">Add new version to a file</button>


	</div>





	<div id="packageManager" title="Package Manager">

		<!-- <a href = "#"  id = "createUpdateFile" >&nbsp;Create new update file&nbsp;</a> -->
		<button id="createPackageButton" class="portalButtons">
			Create new package</button>
		<br>
		<hr>
		<br> <input type="text" id="searchRP" class="addInput"
			onkeyup="getUFAutoFil(this.value)" /> <br>

		<button id="searchRPButton" onclick="getRPinfo()"
			class="portalButtons">Release package information</button>


		<table id="packageTable">
			<tr>
				<td>Type</td>
				<td id="typeRP" class=""></td>
			</tr>
			<tr>
				<td>Name</td>
				<td id="nameRP" class=""></td>
			</tr>
			<tr>
				<td>Version</td>
				<td id="versRP" class=""></td>
			</tr>
			<tr>
				<td>Package</td>
				<td id="packageIdRP" class=""></td>
			</tr>
		</table>


		<!--"data":{"uuid":"2df21282-f8c8-4d2d-a5c4-249a064d55f3","description":null,"name":4,"type":"type","provider":"provider","versions":[["4.0.0","4.1.0"]]}-->

         <script type="text/javascript">
			var uuid = $( "#packageIdRP" ).val();
		</script>

         

		<button id="addNewVersionToRPBtn" onclick="showRPVersionTab(uuid)"
			class="portalButtons">Add new version to a package</button>

	</div>


	<div id="widgetFour"></div>

	<div id="widgetFive"></div>

	<div id="widgetSix"></div>

	<!--  ErrorTab -->

	<div id="errorTab">

		&nbsp;<img src="img/close-button.png" id="errorTabCloseButton"
			class="closeButtons" onclick="$('#errorTab').hide()"><br>
		<img src="img/errorExc.png" id="errorExc">

		<div id="errorTabInnerDiv"></div>

	</div>


	<div id="verAuxDiv">

		<div id="innerVersionDiv">

			<p id="innerVersionDivInfo">Version 1.1.1</p>

			<p id="remVersion">&nbsp; Remove this version &nbsp;</p>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

			<p id="updFile" onclick="updateToReadyVersion()">&nbsp; Update a
				file to this version &nbsp;</p>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

		</div>

	</div>

	<!--  VALIDATION TAB -->


	<div id="validationTab"></div>



	<!--  FTP URL TAB -->

	<div id="ftpUrlTab">


		&nbsp;<img src="img/close-button.png" id="ftpUrlTabCloseButton"
			class="closeButtons" onclick="$('#ftpUrlTab').hide()"> <br>
		<br>
		<br>



		<div id="decorButton">


			<input type="text" id="decorInput" />



			<button id="innerDecorButton">Choose file</button>



			<input type="file" id="myFile" name="myFile" onchange="sub(this)">


		</div>


		<br>
		<br> <input type="button" id="uploadFile" name="uploadFile"
			value="upload"> <br>

		<progress id="prog" value="0" min="0" max="100"> </progress>

		<br>
		<br>

		<!-- The fileinput-button span is used to style the file input field as button -->

		`
		<!--   <span class="btn btn-success fileinput-button">
        <i class="glyphicon glyphicon-plus"></i>
        <span>Select files...</span>
        <!-- The file input field used as target for the file upload widget
        <input id="fileupload" type="file" name="files[]"  multiple>
    </span>
    <br>
    <br>
    The global progress bar
    <div id="progress" class="progress">
        <div class="progress-bar progress-bar-success"></div>
    </div>
    <!-- The container for the uploaded files 
    <div id="files" class="files"></div>
    <br>   -->

		<script>
			/*jslint unparam: true */
			/*global window, $ */
			/* $(function () {
			  
			 // Change this to the location of your server-side upload handler:
			   
			  var url = window.location.hostname + '/:8080/BackOffice/Servlet';


			  $('#fileupload').fileupload({
			     url: '/BackOffice/Servlet?action=upload',
			     dataType: 'json',
			     done: function (e, data) {
			          
			          $.each(data.result.files, function (index, file) {
			              $('<p/>').text(file.name).appendTo('#files');
			         });
			     },
			          progressall: function (e, data) {
			          var progress = parseInt(data.loaded / data.total * 100, 10);
			          $('#progress .progress-bar').css(
			              'width',
			              progress + '%'
			          );
			      }
			  })
			 */

			//	$.ajax({
			//	  type:"GET",
			//  url: "./Servlet?action=addFile&var="+$('#fileupload').val(),
			//success: function (response){

			//      }
			//}); 
			//});
		</script>


		<div id="resultTab">

			<span id="waitBar"> </span>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

			<button id="updateButton" onclick="makeUpdateOnAFile()">
				PERFORM UPDATE</button>

		</div>



	</div>


	<div id="feedBackTab">

		&nbsp;<img src="img/close-button.png" id="feedTabCloseButton"
			class="closeButtons" onclick="$('#feedBackTab').hide(100)"><br>
		<img src="img/success.png" id="successExc">

		<div id="feedTabInnerDiv"></div>


	</div>





	<div id="additionalVersionTab">
	
	
   <img src="img/close-button.png" id="errorTabCloseButton"
			class="closeButtons" onclick="$('#additionalVersionTab').hide()">
	
	</div>


	<div id="addNewVersionToUf">

		<img src="img/close-button.png" id="closeAddVersionToUf"
			class="closeButtons" onclick="$('#addNewVersionToUf').hide()">
		<p>UUID</p>
		<input type="text" id="uuidUf" class="addInput" />

		<p>Version</p>
		<input type="text" id="versionUf" class="addInput" />

		<p>Description</p>
		<input type="text" id="descriptionUf" class="addInput" /> <br> <a
			href="#" id="createNewUFVersionLink" onclick="createNewUFVersion()"
			class="portalButtons"> Create a new version </a> <br> <br>

	</div>


	<div id="addNewVersionToRP">

		<img src="img/close-button.png" id="closeAddVersionToUf"
			class="closeButtons" onclick="$('#addNewVersionToRP').hide()">
		<p>UUID</p>
		<input type="text" id="uuidUf" class="addInput" />

		<p>VERSION</p>
		<input type="text" id="versionUf" class="addInput" />

		<p>DESCRIPTION</p>
		<input type="text" id="descriptionUf" class="addInput" /> <br> <a
			href="#" id="createNewUFVersionLink" onclick="createNewRPVersion()">
			Create a new version </a> <br> <br>

	</div>

	<!--  addVehicleTab  -->

	<div id="page">

		<div id="containerMain">



			<%@include file="header.txt"%>


			<div id="widgetSpace">



				<div id="auxilDiv"></div>

			</div>


			<div id="footer">

				<div id="blockOne">

					<a href="#">Dashboard</a><br> <a href="#">Reports</a><br>
					<a href="#">Admin</a><br> <a href="#">Documents</a>

				</div>

				<div id="blockTwo">

					<a href="#">Add new Widget</a><br> <a href="#">Help?</a><br>
					<a href="#">Logout</a>

				</div>

				<div id="blockThree"></div>

			</div>


		</div>

	</div>

	<%
		} else {
	%>

	<div id="pleaseLogin">

		<p>Please log in</p>
		<a href="login.jsp" id="enterLink"> ENTER SITE </a>

	</div>

	<%
		}
	%>


</body>

</html>