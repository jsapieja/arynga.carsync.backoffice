
var sessionKey;


function errorFunction(response){
	  
	  if(response.indexOf("Error") != -1){
		  
		  var errorArray = response.split("/");   
		  var message = errorArray[1];
		  
		
		  $( "#errorTab" ).show(2000);
		  $( "#errorTab" ).draggable();
		  $( "#errorTabInnerDiv" ).empty();
		  $( "#errorTabInnerDiv" ).append(message);
		  
		  
		  
		 // setTimeout(function(){$( "#errorTab" ).hide(2000);},3000);
		  
	}
	  
}




function toggleWidget(id , state){
	
	
	     
	if(state == "Activate"){
	
	

	$.ajax({
		
		  type:"GET",
		  url: "./Servlet?action=activateWidget&var="+id,
	      success: function (response){
	    	  
	    	
	        	 
if(response.indexOf("Error") != -1){
	    		  
	    		  var errorArray = response.split("/");   
	    		  var message = errorArray[1];
	    		  
	    		  $( "#errorTab" ).show(2000);
	    		  $( "#errorTab" ).draggable();
	    		  $( "#errorTabInnerDiv" ).empty();
	    		  $( "#errorTabInnerDiv" ).append(message);
	    		//  setTimeout(function(){$( "#errorTab" ).hide(2000);},3000);
	    		  
	    	}  else if (response.trim() == "Success"){
	    		
	    	refreshWidgets();
	    		 
	    	 
	  
	        }
	    	 
	      }
	});
	

	
	} else if (state == "Hide" ){
		
		
	       

		$.ajax({
			
			  type:"GET",
			  url: "./Servlet?action=deactivateWidget&var="+id,
		      success: function (response){
		    	
		        	  
	if(response.indexOf("Error") != -1){
		    		  
		    		  var errorArray = response.split("/");   
		    		  var message = errorArray[1];
		    		  
		    		  $( "#errorTab" ).show(2000);
		    		  $( "#errorTab" ).draggable();
		    		  $( "#errorTabInnerDiv" ).empty();
		    		  $( "#errorTabInnerDiv" ).append(message);
		    		//  setTimeout(function(){$( "#errorTab" ).hide(2000);},3000);
		    		  
		    	} else if (response.trim() == "Success"){
		    		
		    		refreshWidgets();
		    		
		    	
		        }
		    	  
		    	 
		      }
		});
		
	
	}
	
}




function refreshWidgets(){
	
	$.ajax({
		
		  type:"GET",
		  url: "./Servlet?action=refreshWidgets",
	      success: function (response){
	      
	        	  
if(response.indexOf("Error") != -1){
	    		  
	    		  var errorArray = response.split("/");   
	    		  var message = errorArray[1];
	    		  
	    		  $( "#errorTab" ).show(2000);
	    		  $( "#errorTab" ).draggable();
	    		  $( "#errorTabInnerDiv" ).empty();
	    		  $( "#errorTabInnerDiv" ).append(message);
	    		//  setTimeout(function(){$( "#errorTab" ).hide(2000);},3000);
	    		  
	    	} else {
	    	
	       // var obj = new Object();
	       // console.log(response);
	   
	  var obj = new Object();	
	  obj  = jQuery.parseJSON(response);	
	   
	 //  var map = new Map();
	   
	 //  alert(map.keys());
	   
	   
	   var array = new Array();
	   array = Object.keys(obj);
	  // alert(array);
	 
	   $( "#widgetsSummary" ).empty();
	   
	   $( "#widgetsSummary" ).append("<p> Widgets</p><table>");
	   
	   
	   for(var j=0 ; j<4 ;j++){
		   
		   var label = array[j];
		  // alert(obj[label]);
		   
		   if(obj[label] == "t"){
			   obj[label] = "Visible";
			   
		   }else if(obj[label] == "f"){
			   obj[label] = "Hidden";
			   
		   }
		   
		   var action = null; 
		   
		   if(obj[label] == "Visible"){
			   action = "Hide";
			   
		   }else if(obj[label] == "Hidden"){
			   action = "Activate";
			   
		   }
		   
	   $( "#widgetsSummary" ).append("<tr><td>&nbsp;&nbsp;&nbsp;"+array[j]+"&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;"+obj[label]+"&nbsp;&nbsp;&nbsp;</td><td><a href = \"#\"   onclick =  \"toggleWidget('"+array[j]+"','"+action+"')\">&nbsp;&nbsp;&nbsp;"+action+"&nbsp;&nbsp;&nbsp;</a></td></tr>");
	       
	   }
	   
	   
	   $( "#widgetsSummary" ).append("</table>");
	   
	 // alert(obj.Package_Manager);
	    		
	        }
	    	  
	      }
	     
	});
	
}


function validationFunction(elementId , event){
	
	   // alert(elementId);
	
	//alert(( "#"+elementId+"" ).val());
	
	  
	if( $( "#"+elementId+"" ).val() == ""){
		
		var message = "This field cannot be left empty";
		  $( "#validationTab" ).show(1000);
				  $( "#validationTab" ).draggable();
				  $( "#validationTab" ).empty();
			  $( "#validationTab" ).append(message);  
			  
			     var x=$("#"+elementId+"" ).offset().left + $( "#"+elementId+"" ).width() +15;
				 var y=$("#"+elementId+"" ).offset().top;
				 $("#validationTab").css({left:x,top:y});
				 
			setTimeout(function(){   $( "#validationTab" ).empty(); $( "#validationTab" ).hide(1000);},4000);
		
			  event.cancelBubble = true;
			  return false;
	}

	//  if(response.indexOf("Error") != -1){
		  
		//  var errorArray = response.split("/");   
		//  var message = errorArray[1];
		  
		
	//	  $( "#errorTab" ).show(2000);
	//	  $( "#errorTab" ).draggable();
	//	  $( "#errorTabInnerDiv" ).empty();
		//  $( "#errorTabInnerDiv" ).append(message);
		  
		  
		 // setTimeout(function(){$( "#errorTab" ).hide(2000);},3000);
		  
	//}
	  return true;
}


////

function getVehicleInfo(vin) {
	
	$.ajax({
		
		  type:"GET",
		  url: "./Servlet?action=getVehicleInfo&var="+vin,
	      success: function (response){
	    	  var obj = jQuery.parseJSON(response);
	    	  $('#modelOutput').html(obj.manufacturer);
	    	  $('#vinOutput').html(obj.vin);
	    	 
	    	  
	    	  
 if(response.indexOf("Error") != -1){
	    		  
	    		  var errorArray = response.split("/");   
	    		  var message = errorArray[1];
	    		  
	    		  $( "#errorTab" ).show(2000);
	    		  $( "#errorTab" ).draggable();
	    		  $( "#errorTabInnerDiv" ).empty();
	    		  $( "#errorTabInnerDiv" ).append(message);
	    		//  setTimeout(function(){$( "#errorTab" ).hide(2000);},3000);
	    		  
	    	} else {
	    	  
	    
	    	 
	      }
	    	  
	    	 
	      }
	});
		  
}

function getVinsAutoFil(vins) {
	
/*	$.ajax({
		
		  type:"GET",
		  url: "./Servlet?action=getAutofil&var="+vins,
	      success: function (response){
	    	     	  
 if(response.indexOf("Error") != -1){
	    		  
	    		  var errorArray = response.split("/");   
	    		  var message = errorArray[1];
	    		  
	    		  $( "#errorTab" ).show(2000);
	    		  $( "#errorTab" ).draggable();
	    		  $( "#errorTabInnerDiv" ).empty();
	    		  $( "#errorTabInnerDiv" ).append(message);
	    		//  setTimeout(function(){$( "#errorTab" ).hide(2000);},3000);
	    		  
	    	} else {   */
	    		  
	    //	var splitArray = response.split("#"); 
	    	  
	    	$( "#vinSearch" ).autocomplete({
	    	    
	    	      source: "Servlet?action=getAutofil" ,
	    	      selectFirst: true,
	    	      minLength: 0
	    	      
	    	    })
             }
	
	       /*
 
	      }
	});
	
	
}    */


function getUFAutoFil(uf) {
	
	  $( "#searchUF" ).autocomplete({
		  source: "Servlet?action=getUFAutofil" ,
	      selectFirst: true,
	      minLength: 0
	    });
	  
	
/*	$.ajax({
		
		  type:"GET",
		  url: "./Servlet?action=getUFAutofil&var="+uf,
	      success: function (response){
	    	  
	    	// Check if response contains "Error"
	    	  if(response.indexOf("Error") != -1){
	    		  
	    		  var errorArray = response.split("/");   
	    		  var message = errorArray[1];
	    		  
	    		  $( "#errorTab" ).show(2000);
	    		  $( "#errorTab" ).draggable();
	    		  $( "#errorTabInnerDiv" ).empty();
	    		  $( "#errorTabInnerDiv" ).append(message);
	    		 // setTimeout(function(){$( "#errorTab" ).hide(2000);},3000);
	    		  
	    	  }
	    	  
	    	  var splitArray = response.split(" "); 
	    	  
	    	
	      }
	});      */
	
	
}



function addNewVehicle(){
	 
$( "#addVehTab").show();  
		
}

function addNewVehicle(){
		
var  json  = "{\"protocol\":\"1.6\", \"id\": null, \"session\": \""+sessionKey+"\", \"type\":\"AddVehicle\", \"data\":{\"manu-id\":\""+ $('#inputMan').val()+"\", \"model\":\""+ $('#inputModel').val()+"\", \"type\":\""+ $('#inputType').val()+"\", \"year\":\""+ $('#inputYear').val()+"\", \"dom\":\""+ $('#inputDate').val()+"\", \"vin\":\""+ $('#inputVin').val()+"\", \"group name\":\""+ $('#inputGroup').val()+"\", \"description\":\""+ $('#inputDescription').val()+"\"}, \"seq\": 1}";
	
	$.ajax({
		
		  type:"GET",
		  url: "./Servlet?action=addNewVeh&var="+json,
	      success: function (response){
	    	  var obj = jQuery.parseJSON(response);
	    	 // $('#modelOutput').html(obj.manufacturer);
	    	 // $('#vinOutput').html(obj.vin);
	         
	      }
	}); 
	
	
}



function removeBoat(vin){
	
var  json  = "{\"protocol\":\"1.6\", \"id\": null, \"session\":\""+sessionKey+"\", \"type\":\"RemoveVehicle\", \"data\":{\"vin\":\""+vin+"\"}, \"seq\": 1}";
	
	$.ajax({
		
		  type:"GET",
		  url: "./Servlet?action=removeVeh&var="+json,
	      success: function (response){
	    	//  var obj = jQuery.parseJSON(response);
	    	  
	    	  if(response.indexOf("Success") != -1){
	    		 
	    		  var successArray = response.split("/");   
	    		  var message = successArray[1];
	    		  $( "#feedBackTab" ).show(1000);
	 
	    		  $( "#feedBackTab" ).draggable();
	    		  $( "#feedTabInnerDiv" ).empty();
	    		  $( "#feedTabInnerDiv" ).append(message);
	    		 // setTimeout(function(){$( "#errorTab" ).hide(2000);},3000);
	    		  
	    	} else if(response.indexOf("Error") != -1){
	    		
	    		var errorArray = response.split("/");   
	    		  var message = errorArray[1];
	    		  $( "#errorTab" ).show(2000);
	 
	    		  $( "#errorTab" ).draggable();
	    		  $( "#errorTabInnerDiv" ).empty();
	    		  $( "#errorTabInnerDiv" ).append(message);
	    		
	    		
	    	}
	    	  
	    	  
	    	 // $('#modelOutput').html(obj.manufacturer);
	    	 // $('#vinOutput').html(obj.vin);
	         
	      }
	}); 
	
	
}




function  createNewUF(){
	
var  json  = "{\"protocol\":\"1.6\", \"id\": null, \"session\":\""+sessionKey+"\", \"type\":\"NewUF\", \"data\":{\"name\":\""+ $('#inputUFName').val()+"\", \"description\":\""+ $('#inputUFDesc').val()+"\", \"type\":\""+ $('#inputUFType').val()+"\", \"provider\":\""+ $('#inputUFProvider').val()+"\"}, \"seq\": 1}";
	
	$.ajax({
		
		  type:"GET",
		  url: "./Servlet?action=createNewUF&var="+json,
	      success: function (response){
	    	  var obj = jQuery.parseJSON(response);
	    	 // $('#modelOutput').html(obj.manufacturer);
	    	 // $('#vinOutput').html(obj.vin);
	         
	      }
	});   
   	
	
}



function  getUFinfo(){
	// searchUFButton
	var value = $('#searchUF').val();
	value = value.trim();
var  json  = "{\"protocol\":\"1.6\", \"id\": null, \"session\": \""+sessionKey+"\", \"type\":\"UFInfo\", \"data\":{\"uuid\":\""+value+"\"}, \"seq\": 1}";
	
	$.ajax({
		  type:"GET",
		  url: "./Servlet?action=getUFinfo&var="+json,
	      success: function (response){
	    	  
	    	  var value = $('#searchUF').val();
	    		value = value.trim();
	    	  
	    	 // var obj = jQuery.parseJSON(response);
	    	 // $('#modelOutput').html(obj.manufacturer);
	    	 // $('#vinOutput').html(obj.vin);  
	    	  var splitArray = response.split("#");   
	    	  $('#desc').html(splitArray[1]);
	    	  $('#name').html(splitArray[2]);
	    	  $('#type').html(splitArray[3]);
	    	  $('#prov').html(splitArray[4]);
	    	  
	    	  
	    	  var objectVersion = new Array();
	    	  
	    	  objectVersion = JSON.parse(splitArray[5]);
	    	  
	    	  console.log(objectVersion[0].length);
	    	  
	    	  if(objectVersion[0].length > 3){
	    		  
	    		var  objectVersionParam = JSON.stringify(objectVersion);
	    	//	alert(objectVersionParam);
	    		
	    		objectVerionParam = objectVersionParam.replace( /"/g,'#');
	    		
	    		objectVerionParam = '\"'+objectVerionParam+'\"';
	    	//	alert(objectVerionParam);
	    	  
	    	  $('#versi').html(objectVersion[0][0]+ "&nbsp;" + objectVersion[0][1] + "&nbsp;&nbsp;&nbsp;<a href = \"#\"  onclick = 'showAdditionalVersionsTab("+objectVerionParam+" , \""+value+"\")'>&nbsp;More . . .</a>");
	    	  
	    	  } ;
	    	  uuid = splitArray[0];
	    	  
	    	  $('#addNewVersionToUFFile').show();
	      
	      }
	});   
   		
}


function validateSearchVin(){
	
	
        $(".error").hide();
        var hasError = false;
        var searchReg = /^[a-zA-Z0-9-]+$/;
        var searchVal = $("#vinSearch").val();
        if(searchVal == '') {
            $("#searchImage").after('<span class="error">Please enter a search term.</span>');
            hasError = true;
        } else if(!searchReg.test(searchVal)) {
            $("#searchImage").after('<span class="error">Enter valid text.</span>');
            hasError = true;
        }
        if(hasError == true) {return false;}
   
        
}


    

 function  getRPinfo(){
	 
	 //searchRP
	 $( "#searchRP" ).val();
	
 var  json  = "{\"protocol\":\"1.6\", \"id\": null, \"session\": \""+sessionKey+"\", \"type\":\"RPInfo\",\"data\":{\"uuid\":\"4f6e513d-ba2c-4307-82e1-1f43a5ba9b71\",\"version\":\"4.2.0\"}, \"seq\": 1}";
	
//\"manu-id\":\"cd307a9a-14e8-4e45-b474-1fc95d476f2d\"
    // "uuid\":\"4f6e513d-ba2c-4307-82e1-1f43a5ba9b71
// Check if response contains "Error"

	$.ajax({
		
		  type:"GET",
		  url: "./Servlet?action=getRPinfo&var="+json,
	      success: function (response){
	    	 // var obj = jQuery.parseJSON(response);
	    	 // $('#modelOutput').html(obj.manufacturer);
	    	 // $('#vinOutput').html(obj.vin);
	    	  
	    	  if(response.indexOf("Error") != -1){
	    		  
	    		  var errorArray = response.split("/");   
	    		  var message = errorArray[1];
	    		  $( "#errorTab" ).show(2000);
	 
	    		  $( "#errorTab" ).draggable();
	    		  $( "#errorTabInnerDiv" ).empty();
	    		  $( "#errorTabInnerDiv" ).append(message);
	    		 // setTimeout(function(){$( "#errorTab" ).hide(2000);},3000);
	    		  
	    	}  else {
	    	  
	    	//  var splitArray = response.split("#");   
	    	//  $('#typeRP').html(splitArray[0]);
	    	//  $('#nameRP').html(splitArray[1]);
	    	//  $('#versRP').html(splitArray[2]);
	    	//  $('#packageIdRP').html(splitArray[3]);
	    	
	    	  var obj = new Object();
	    	  obj = JSON.parse(response);
	    	 
	    	  $('#typeRP').html(obj.type);
		      $('#nameRP').html(obj.description);
		      $('#versRP').html(obj.version);
		    //  $('#packageIdRP').html(obj.uuid);
	    	  
	    	}
	    	   
	      }
	});   
	
  $('#addNewVersionToRPBtn').show();
   		
}




function setVBSip(){
	
//	$('#setVBSipButton').val(obj.vin);
	
	
var  ip  = $('#setVBSip').val();
	
	$.ajax({
		
		  type:"GET",
		  url: "./Servlet?action=setVBSAddress&var="+ip,
	      success: function (response){
	    	  var obj = jQuery.parseJSON(response);
	    	 // $('#modelOutput').html(obj.manufacturer);
	    	 // $('#vinOutput').html(obj.vin);
	         
	      }
	});   
   	
	
	
}


function setSessionKey(){
	
var  key  = $('#setKey').val();


	$.ajax({
		
		  type:"GET",
		  url: "./Servlet?action=setSessionKey&var="+key,
	  
	});   
   	
	
	
//	$('#setSessionKey').val(obj.vin);
	
}
	
	
	
function getSessionKey(){
	
	var  key  = $('#setSessionKey').val();


		$.ajax({
			
			  type:"GET",
			  url: "./Servlet?action=getSessionKey&var="+key,
		      success: function (response){
		    	  sessionKey = response;
		          //alert(sessionKey);
		         
		      }
		});   
	   	
		
		
//		$('#setSessionKey').val(obj.vin);
		
	}	


function addNewPackage(){
	
	   /*
	    *    3.27  Create new release package .
	    */
	
	
	      // manu-id needs to be made dynamic .
	
	//     $( "#feedTabInnerDiv" ).empty();
	//     $( "#feedTabInnerDiv" ).append(message);
	
	var  json  = "{\"protocol\":\"1.6\", \"id\": null, \"session\":\""+sessionKey+"\", \"type\":\"RPCreate\", \"data\":{\"name\":\""+ $('#packageName').val()+"\", \"type\":\""+ $('#packageType').val()+"\", \"description\":\""+ $('#packageDescription').val()+"\" , \"manu-id\": \"cd307a9a-14e8-4e45-b474-1fc95d476f2d\"}, \"seq\": 1}";


	$.ajax({
		
		  type:"GET",
		  url: "./Servlet?action=addNewPackage&var="+json,
	      success: function (response){
	    	  
	    	  var obj = new Object();	
	    	  
	    	  obj  = jQuery.parseJSON(response);
	    	  
	    	// alert(Object.keys(obj));
	    	 //type , name , 77d2c853-d78
	    	  
	    	  $( "#feedBackTab" ).show(1000);
	    		 
    		  $( "#feedBackTab" ).draggable();
    		  $( "#feedTabInnerDiv" ).empty();
    		  $( "#feedTabInnerDiv" ).append("The package was created. The details of the package are :<br> name  :"+obj.name+" ,<br> type  :"+obj.type+" ,<br> uuid  :"+obj.uuid+"");
	    	  
	      }
	});   
   	
	
	
	
}



function showUfVersionTab(uuid) {
	
	//addNewVersionToUf
	 $('#addNewVersionToUf').draggable();
	 $('#addNewVersionToUf').show(800);
	 $('#uuidUf').val(uuid);
   		
}


function showAdditionalVersionsTab(objectArrayParam , fileId) {
	
	//addNewVersionToUf
	
	objectArrayParam = objectArrayParam.replace( /#/g,'"');
	
	var ver =  new Array();
	
	ver = objectArrayParam.split(",");
	
	//objectVersion = JSON.parse(objectArrayParam);
	// var array = objectArray[0];
	
  // var objectArray = objectArrayParam[0];
    
 //   alert("test23" +  ver[0] );
	 
	/// $( "#additionalVersionTab" ).empty();
	
	$("#additionalVersionTab").append("<p> File versions </p>" + "  File uuid :  "+fileId +"<br>");
	
	 $("#additionalVersionTab").append('<br>');
	
	 for(var i = 0 ; i <  ver.length ; i++ ){
		 
		 ver[i] = ver[i].replace( /"/g,'');
		 ver[i] = ver[i].replace( /]]/g,'');
		 ver[i] = ver[i].replace( /\[\[/g,'');
	
	
		 
	    $("#additionalVersionTab").append("<a href = \"#\" onclick = \"verAuxTab()\"> "+ ver[i]+"</a> , ");
	    if(i%5 == 0) {
	    	 $("#additionalVersionTab").append('<br>');
	    	
	    }
		 
	 }
	
	
	 $('#additionalVersionTab').draggable();
	 $('#additionalVersionTab').show(800);
	 
	 
	
	 var x=$("#addNewVersionToUFFile").offset().left;
	 var y=$("#addNewVersionToUFFile").offset().top;
	 $("#additionalVersionTab").css({left:x,top:y});
	     		
}


function  runErrorMessage(error){

	  if(error == "one") {
	  error = "The credentials could not have been checked. the database is most probably not started.";  
	  } else if (error == "two"){
	  error = "You have entered the wrong credentials.";    
	  }
	
	  $( "#errorTab" ).show(2000);
	  $( "#errorTab" ).draggable();
	  $( "#errorTabInnerDiv" ).empty();
	  $( "#errorTabInnerDiv" ).append(error);
	  	
}


function createNewUFVersion(){
	
	/*
	 *                
	 *  
	 * 
	 */
	
    var json = "{\"protocol\":\"1.6\",\"id\": null, \"session\":\""+sessionKey+"\", \"type\":\"UFAddVersion\", \"data\":{\"uuid\":\""+ $('#uuidUf').val()+"\", \"version\":\""+ $('#versionUf').val()+"\", \"description\":\""+ $('#descriptionUf').val()+"\"}, \"seq\": 1}";

	$.ajax({
		
		  type:"GET",
		  url: "./Servlet?action=addNewVersionToUfFIle&var="+json,
	      success: function (response){
	    	  
 if(response.indexOf("Error") != -1){
	    		  
	    		  var errorArray = response.split("/");   
	    		  var message = errorArray[1];
	    		  
	    		
	    		  $( "#errorTab" ).show(2000);
	    		  $( "#errorTab" ).draggable();
	    		  $( "#errorTabInnerDiv" ).empty();
	    		  $( "#errorTabInnerDiv" ).append(message);
	    		 // setTimeout(function(){$( "#errorTab" ).hide(2000);},3000);
	    		  
	    	} else {
	    		
	    		 $( "#ftpUrlTab" ).show(2000);
	    		 $( "#ftpUrlTab" ).draggable();
	    		 $( "#ftpUrlTab" ).append(response);
	    		
	    	}
	    	  
	      }
	});   
	
	
}

function sub(obj) {
	
		    var file = obj.value;
		    var fileName = file.split("\\");
		    document.getElementById("decorInput").value = fileName[fileName.length-1];
		  
		    event.preventDefault();
		 	
            }

function showRPVersionTab(uuid){
	
	//addNewVersionToRP
	 $( "#addNewVersionToRP" ).show(800);
	 $( "#addNewVersionToRP" ).draggable();
	 
     $('#uuidUf').val(uuid) ;
	
}




function  verAuxTab(){
	
	//verAuxDiv
	 $( "#verAuxDiv" ).show(800);
	 $( "#verAuxDiv" ).draggable();
	
}


function updateToReadyVersion(){
	
	 $( "#ftpUrlTab" ).show(2000);
	 $( "#ftpUrlTab" ).draggable();
	
		
}


function openwidgetSummary(){
	
	 $( "#widgetsSummary" ).fadeIn(1000);
	 $( "#widgetsSummary" ).draggable();
	
	
}


function createNewRPVersion(){
	
	
	/*
	 *    Crete new RP version , message    
	 *  
	 */
	
    var json = "{\"protocol\":\"1.6\",\"id\": null, \"session\":\""+sessionKey+"\", \"type\":\"UFAddVersion\", \"data\":{\"uuid\":\""+ $('#uuidUf').val()+"\", \"version\":\""+ $('#versionUf').val()+"\", \"description\":\""+ $('#descriptionUf').val()+"\"}, \"seq\": 1}";

	$.ajax({
		
		  type:"GET",
		  url: "./Servlet?action=addNewVersionToUfFIle&var="+json,
	      success: function (response){
	    	  
 if(response.indexOf("Error") != -1){
	    		  
	    		  var errorArray = response.split("/");   
	    		  var message = errorArray[1];
	    		  
	    		
	    		  $( "#errorTab" ).show(2000);
	    		  $( "#errorTab" ).draggable();
	    		  $( "#errorTabInnerDiv" ).empty();
	    		  $( "#errorTabInnerDiv" ).append(message);
	    		 // setTimeout(function(){$( "#errorTab" ).hide(2000);},3000);
	    		  
	    	} else {
	    		
	    		 $( "#ftpUrlTab" ).show(2000);
	    		 $( "#ftpUrlTab" ).draggable();
	    		 $( "#ftpUrlTab" ).append(response);
	    		
	    	}
	    	  
	      }
	});   
	
	
}

function   makeUpdateOnAFile(){
	
	 var json = "none";
	 
	$.ajax({
		
		  type:"GET",
		  
		  url: "./Servlet?action=makeUpdateOnAFile&var="+json,
		  
	      success: function (response){
	    	  
if(response.indexOf("Error") != -1){
	    		  
	    		  var errorArray = response.split("/");   
	    		  var message = errorArray[1];
	    		 
	    		  $( "#errorTab" ).show(2000);
	    		  $( "#errorTab" ).draggable();
	    		  $( "#errorTabInnerDiv" ).empty();
	    		  $( "#errorTabInnerDiv" ).append(message);
	    		 // setTimeout(function(){$( "#errorTab" ).hide(2000);},3000);
	    		  
	    	} else {
	    		
	    		 $( "#ftpUrlTab" ).show(2000);
	    		 $( "#ftpUrlTab" ).draggable();
	    		 $( "#ftpUrlTab" ).append(response);
	    		
	    	}
	    	  
	      }
	});   
	
	
}


function logOut(){
	
	$.ajax({
		
		  type:"GET",
		  url: "./Servlet?action=logOut",
	      success: function (response){
	    	  
if(response.indexOf("Error") != -1){
	    		  
	    		  var errorArray = response.split("/");   
	    		  var message = errorArray[1];
	    		  
	    		
	    		  $( "#errorTab" ).show(2000);
	    		  $( "#errorTab" ).draggable();
	    		  $( "#errorTabInnerDiv" ).empty();
	    		  $( "#errorTabInnerDiv" ).append(message);
	    		 // setTimeout(function(){$( "#errorTab" ).hide(2000);},3000);
	    		  
	    	} else {
	    		
	    	
	    		
	    	}
	    	  
	      }
	});   
	
	
}
	
	
	
	
	
	
	
	
	




	
	  
    