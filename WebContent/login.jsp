<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" %>
    
      
<!DOCTYPE html>

<html>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">


<link rel="stylesheet" type="text/css" href="styles/CarSyncStyles.css"/>

<link rel="stylesheet" href="styles/jquery-ui.css" />

<script type="text/javascript" src="javascript/jquery/jquery-1.10.2.min.js"> </script>
<script type="text/javascript" src="javascript/jquery-ui-1.10.3/ui/jquery-ui.js"></script>


<script type="text/javascript" src="javascript/CarSyncJS.js"></script>


<style type = "text/css">


</style>

<title>Synchronization Portal Login</title>

</head>

<body>


<script type = "text/javascript">

window.logOut();

</script>


<div id = "errorTab">

 &nbsp;<img src = "img/close-button.png" id = "errorTabCloseButton" alt = "close" class = "closeButtons"   onclick = "$('#errorTab').hide()"><br>
<img src = "img/errorExc.png" alt = "error"  id = "errorExc">
 
<div id  = "errorTabInnerDiv"> </div>
 
</div>


<%   

String passed = null;

String error = null;

passed = (String)request.getAttribute("passed");
  
if( passed != null   &&  passed.equals("no")){
    
	 error = (String)request.getAttribute("error");
	 
	 System.out.println("Error  is " + error);
	 
	 %>  
	 
	  <script type="text/javascript">

	  var parameter = "<%=error%>";
	
	  window.runErrorMessage(parameter);
	 
   
	  </script>
	 
	  <%
	 
};

request.setAttribute("error",null);
request.setAttribute("passed",null);

%>


<div id = "loginDiv">
<img src   =  "img/arynga_logo_small.png"  alt = "logo_small_arynga"  id = "aryngaLogoLogin">           
<div class="login">
 
<hr id = "hr1"><br>
    
<h1 id = "hLogin">Login</h1><br>

<hr id = "hr2">	

<form method="post" action = "Servlet">

<input type="text" name="userInput" placeholder="Username" required="required" class = "loginInput" /><br><br>

<input type="password" name="passwordInput" placeholder="Password" required="required"   class = "loginInput" />

<button type="submit"  id = "LoginBTn" class="btn btn-primary btn-block btn-large">Enter</button>


    </form>
    
    </div>
    
  <!--   <img src   =  "img/vessel_pic.png"   alt = "vessel picture"  id = "vesselPic">  -->
   <img src   =  "img/carsync_logo.jpg"   alt = "carsync logo"  id = "carSyncPic">
   </div>

</body>
</html>