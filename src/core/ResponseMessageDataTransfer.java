package core;

public class ResponseMessageDataTransfer {
	
	private  String protocol = "";
	private  String session = "";
	private  String type = "";
	private  String status = "";
	private  String status_str = ""; 
	private  Object data = "";
	private  String respToSeq = "";
	private  String respToType = "";
	
	
	public ResponseMessageDataTransfer(String protocol, String session,
			String type, String status, String status_str, Object data,
			String respToSeq, String respToType) {

		
		this.protocol = protocol;
		this.session = session;
		this.type = type;
		this.status = status;
		this.status_str = status_str;
		this.data = data;
		this.respToSeq = respToSeq;
		this.respToType = respToType;
		
		
		
	}
	
	
	
	public ResponseMessageDataTransfer(){
		
		
		
		
	}
	
	public String getProtocol() {
		return protocol;
	}
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
	public String getSession() {
		return session;
	}
	public void setSession(String session) {
		this.session = session;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatus_str() {
		return status_str;
	}
	public void setStatus_str(String status_str) {
		this.status_str = status_str;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public String getRespToSeq() {
		return respToSeq;
	}
	public void setRespToSeq(String respToSeq) {
		this.respToSeq = respToSeq;
	}
	public String getRespToType() {
		return respToType;
	}
	public void setRespToType(String respToType) {
		this.respToType = respToType;
	}

}
