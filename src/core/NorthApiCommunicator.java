package core;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cedarsoftware.util.io.JsonReader;
import com.cedarsoftware.util.io.JsonWriter;

import org.apache.commons.net.ftp.FTP;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;


public class NorthApiCommunicator {
	

	 String address = null;
	 
	   public NorthApiCommunicator( HttpSession session ){
		        System.out.println("session vbs addrr  :  "+(String)session.getAttribute("vbsAddress"));
		        this.address = (String)session.getAttribute("vbsAddress");
		        }
	   
	   
	   /**
	    *   The method for creating a manufacturer on a vehicle backend server .    
	    *   @author Jacek Sapieja
	    *   
	    */
	   
	  
	   public ResponseMessageDataTransfer sendMessage(RequestMessageDataTransfer dto)    {
		   
		//   Object obj = null;
		   
		ResponseMessageDataTransfer responseDto = null; 
		   
		          
		   try {
			   
				
				//http://204.68.122.215:8080/app-proc");
			   
			   
			   System.out.println("Trying to open connection to " +address);
			   
			   
			   
			    URL servConn = new URL("http://"+ address);						
			// http://204.68.122.215/package-upload/2df21282-f8c8-4d2d-a5c4-249a064d55f3-vfvfvfvfv
			  // URL servConn = new URL("http://204.68.122.215/package-upload/2df21282-f8c8-4d2d-a5c4-249a064d55f3-vfvfvfvfv");
				
				HttpURLConnection conn  = (HttpURLConnection) servConn.openConnection();
				
				System.out.println("Connected to :   "+conn.getURL());
				
				conn.setDoOutput(true);
				conn.setDoInput(true);
				
				
				//conn.setUseCaches(false);
				//conn.setDefaultUseCaches(false);
				
				conn.setRequestProperty("Content-Type", "application/json");
				conn.setRequestProperty("Accept", "application/json");
				conn.setRequestMethod("POST");

			
			 //    File firstLocalFile = new File("C:/Users/T430/Documents/MadisonMockup.jpg");
				 
		        //    String firstRemoteFile = "MadisonMockup.jpg";
		         //   InputStream inputStream = new FileInputStream(firstLocalFile);					
				 //String host = request.getRemoteHost();
				 
			    //response.setContentType("application/json");
				 
				 
		         OutputStream outputToNorthApi = conn.getOutputStream();
		       //  BufferedInputStream bfis = new BufferedInputStream(inputStream);
		      //   byte[] buffer = new byte[1024];
		      //   int bufferLength = 0;

		         // now, read through the input buffer and write the contents to the
		         // file
		       //  while ((bufferLength = bfis.read(buffer)) > 0) {
		       // 	 outputToNorthApi.write(buffer, 0, bufferLength);

		      //   }
		         
		      ObjectMapper mapper = new ObjectMapper();
		     	
		     	
	         	mapper.writeValue( outputToNorthApi, dto);
	    	
		      
		    	try {
		      
		     		// convert user object to json string, and save to a file
		     	
		     		// display to console
		     		
		    		System.out.println(mapper.writeValueAsString(dto));
		      
		    	} catch (JsonGenerationException e) {
		      
		    		e.printStackTrace();
		      
		   	} catch (JsonMappingException e) {
		      
		    		e.printStackTrace();
		     
	   	} catch (IOException e) {
		      
		   		e.printStackTrace();
		      
		    	}
		         
		      
		         
		         /*
		          *    write a java object with message to jsonWriter
		          * 
		          */
		         
	 
		         outputToNorthApi.flush(); 		         
		         outputToNorthApi.close();
				
		
		         java.io.BufferedReader br = new java.io.BufferedReader(new java.io.InputStreamReader(conn.getInputStream()));
		         java.lang.StringBuffer sb = new java.lang.StringBuffer();
		         java.lang.String str = br.readLine();
		         while(str != null){
		               sb.append(str);
		               str = br.readLine();
		         }
		         
		         br.close();
		         
		         java.lang.String responseString = sb.toString();
		         
		         System.out.println(responseString);
		         
		       //  responseDto = mapper.readValue(responseString , ResponseMessageDataTransfer.class);
				
			     } catch (IOException e1) {
										// TODO Auto-generated catch block
				e1.printStackTrace();
			  
				}
		   
		      
		 return responseDto;
		       
	    }
	   
	   
	   public ResponseMessageDataTransfer sendMessageUsingJson(String jsonString)    {
		   
			//Object obj = null;
			   
			ResponseMessageDataTransfer responseDto = null; 
			   
			          
			   try {
				  
					//http://204.68.122.215:8080/app-proc");
				   
				   System.out.println("Trying to open connection to " +address);
				   URL servConn = new URL("http://"+ address);						
					// http://204.68.122.215/package-upload/2df21282-f8c8-4d2d-a5c4-249a064d55f3-vfvfvfvfv
					 //  URL servConn = new URL("http://204.68.122.215/package-upload/2df21282-f8c8-4d2d-a5c4-249a064d55f3-vfvfvfvfv");
					
					HttpURLConnection conn  = (HttpURLConnection) servConn.openConnection();
					
					System.out.println("Connected to :"+conn.getURL());
											
				
					conn.setDoOutput(true);
					conn.setDoInput(true);
					
					//conn.setUseCaches(false);
					//conn.setDefaultUseCaches(false);
					
					conn.setRequestProperty("Content-Type", "application/json");
					conn.setRequestProperty("Accept", "application/json");
					conn.setRequestMethod("POST");
					
					 //String host = request.getRemoteHost();
					 
				    // response.setContentType("application/json");
					 
					 
			         OutputStream outputToNorthApi = conn.getOutputStream();
			         
			         /*
			          *    write a java object with message to jsonWriter
			          * 
			          */
			         
			         outputToNorthApi.write(jsonString.getBytes());
		             
			         outputToNorthApi.flush(); 		         
			         outputToNorthApi.close();
					
			
			         java.io.BufferedReader br = new java.io.BufferedReader(new java.io.InputStreamReader(conn.getInputStream()));
			         java.lang.StringBuffer sb = new java.lang.StringBuffer();
			         java.lang.String str = br.readLine();
			         while(str != null){
			               sb.append(str);
			               str = br.readLine();
			         }
			         
			         br.close();
			         
			         java.lang.String responseString = sb.toString();
			         
			         System.out.println(responseString);
			         
			    	 ObjectMapper mapper = new ObjectMapper();
			         
			         responseDto = mapper.readValue(responseString , ResponseMessageDataTransfer.class);
					
				     } catch (IOException e1) {
											// TODO Auto-generated catch block
					  e1.printStackTrace();
				  
					}
			   
			      
			 return responseDto;
			       
		    }
	   

          }
