package core;

public class RequestMessageDataTransfer {
	
	private  String  protocol = null;
	private  String   id  =  null;
	private  String   session = null;
	private  String  type = null;
	private  Object  data =  null;
	private  String   seq =  null;
	  
		public RequestMessageDataTransfer(String protocol, String id,
				String session, String type, Object data, String seq) {
			super();
			this.protocol = protocol;
			this.id = id;
			this.session = session;
			this.type = type;
			this.data = data;
			this.seq = seq;
		}
	  
	  
	public String getProtocol() {
		return protocol;
	}
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	public String getSession() {
		return session;
	}
	public void setSession(String session) {
		this.session = session;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Object getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	

}
