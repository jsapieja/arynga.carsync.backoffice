package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class DbBean {
	
	
	    private final static DbBean dbInstance = new DbBean();
	  
	    public static DbBean getInstance() {
	    	
	        return dbInstance;
	    }
	
	String myDataField = null;
	Connection myConnection = null;
	PreparedStatement myPreparedStatement = null;
	String test = "test";
	
   private DbBean(){
	
	try {
		
		
		String driver = "org.postgresql.Driver";
		String url = "jdbc:postgresql://localhost:5432/carSyncBackofficeDb";
		String username = "admin";
		String password = "qwerty12";
		//carSyncBackofficeDb
		
		try {
			Class.forName(driver).newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			myConnection = DriverManager.getConnection(url,username,password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	
	
		
		}
		catch(ClassNotFoundException e)
		
		{e.printStackTrace();
		
		}
	
		}	
   
       /*
        *    The method verifying login credentials
        *    @author Jacek Sapieja
        *     
        */
   
        public boolean verifyCredentials(String inputName, String inputPass , HttpServletRequest request){
        	 
        	System.out.println("Verify creds was ran");
        	
        	
        Statement stmt = null;
        
		try {
			stmt = myConnection.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch bloc
			System.out.println("Database is not started");
			e.printStackTrace();
		}
		
        try {
        	
			ResultSet rs = stmt.executeQuery("SELECT user_login , user_password FROM users");
			
			while(rs.next()){
				
			String	dbName = rs.getString("user_login");
		    String 	dbPass = rs.getString("user_password");
		    
		    
		    if(dbName.equals(inputName)){
		    	
		    	System.out.println("name passed");
		    
		    if(dbPass.equals(inputPass)){
		    	
		    	
		    	 System.out.println("password ok");
		    	 
		    	 HttpSession session = request.getSession(true);
		    	 session.setAttribute("user", dbName);
		    	
		    	 return true;
		    	
		    } else {
		    	
		    	 
		    	 return false;
		    	
		    }
				
		    
		    }
		    
		    
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        	
			return false;
        		        	
        }
        
        
        public  void  setKey(String key){
       	 
            Statement stmt = null;
            
    		try {
    			stmt = myConnection.createStatement();
    		} catch (SQLException e) {
    			// TODO Auto-generated catch bloc
    			System.out.println("Database is not started");
    			e.printStackTrace();
    		}
    		
            try {
            	
    			int i = stmt.executeUpdate("INSERT INTO system_settings (key) VALUES ('"+key+"');");
    			
    			
    			
    		} catch (SQLException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
            	
    			            		        	
            }
        
        
        
       
        public  void  setAddrrInDb(String address){
          	 
            Statement stmt = null;
            
    		try {
    			stmt = myConnection.createStatement();
    		} catch (SQLException e) {
    			// TODO Auto-generated catch bloc
    			System.out.println("Database is not started");
    			e.printStackTrace();
    		}
    		
            try {
            	
    			int i = stmt.executeUpdate("INSERT INTO system_settings (ipaddress) VALUES ('"+address+"');");
    			
    			
    			
    		} catch (SQLException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
            	
    			            		        	
            }
        
        
        
        public Map<String, String> printWidgetState(){
        	Map<String , String> m = new HashMap<String, String>();
        	System.out.println("Verify creds was ran");
        	
            Statement stmt = null;
            
    		try {
    			stmt = myConnection.createStatement();
    		} catch (SQLException e) {
    			// TODO Auto-generated catch bloc
    			System.out.println("Database is not started");
    			e.printStackTrace();
    		}
    		
            try {
            	
    			ResultSet rs = stmt.executeQuery("SELECT widget_id , widget_name , active FROM widgets ");
    			
    			
    			
    			while(rs.next()){
    				
    				String widName = rs.getString("widget_name");
    				
    				String active = rs.getString("active");
    				
    				m.put(widName,active);
    				
    			}
    			
    	
    		} catch (SQLException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
			return m;
            	
    	  	
          }
        
        
        
        
        
        
    public void deactivateWidget(String wname){
        	
        	Map<String , String> m = new HashMap<String, String>();
        
            Statement stmt = null;
            
    		try {
    			stmt = myConnection.createStatement();
    		} catch (SQLException e) {
    			// TODO Auto-generated catch bloc
    			System.out.println("Database is not started");
    			e.printStackTrace();
    		}
    		
            try {
            	System.out.println("deac widget" + wname);
    			int rs = stmt.executeUpdate("UPDATE widgets SET active = 'false' WHERE widget_name ='"+wname+"'");
    		
    		} catch (SQLException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
		
          }
        
        
        
        
  public void activateWidget(String wname){
	
	 
        	
        	Map<String , String> m = new HashMap<String, String>();
        
            Statement stmt = null;
            
    		try {
    			stmt = myConnection.createStatement();
    		} catch (SQLException e) {
    			// TODO Auto-generated catch bloc
    			System.out.println("Database is not started");
    			e.printStackTrace();
    		}
    		
            try {
            	
    			int rs = stmt.executeUpdate("UPDATE widgets SET active = 'true' WHERE widget_name ='"+wname+"';");
    		
    		} catch (SQLException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
		
          }
        
        
       
        public  String  getKey(){
          	 
            Statement stmt = null;
            String	key = "";
            
    		try {
    			stmt = myConnection.createStatement();
    		} catch (SQLException e) {
    			// TODO Auto-generated catch bloc
    			System.out.println("Database is not started");
    			e.printStackTrace();
    		}
    		
            try {
            	
    			ResultSet rs = stmt.executeQuery("SELECT key FROM system_settings");
    			
    			while(rs.next()){
    				
    			if(rs.getString("key") != null && rs.getString("key")!= ""){	
    				
    			key = rs.getString("key");
    		    
    			}
    		    

    		    }
    		    
    		  	
    			
    		} catch (SQLException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
            	
            
          
    			return key;
            		        	
    			            		        	
            }
        
        
        
        
        public  String  getIpAddrr(){
          	 
            Statement stmt = null;
            String	ad = "";
            
    		try {
    			stmt = myConnection.createStatement();
    		} catch (SQLException e) {
    			// TODO Auto-generated catch bloc
    			System.out.println("Database is not started");
    			e.printStackTrace();
    		}
    		
            try {
            	
    			ResultSet rs = stmt.executeQuery("SELECT ipaddress FROM system_settings");
    			
    			while(rs.next()){
    				
    				
    		    if (rs.getString("ipaddress")!= null){		
    			ad = rs.getString("ipaddress");
    		    }
    		    System.out.println("address from db"+ad);
    		   
    		    }
    		    
    		  	
    			
    		} catch (SQLException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
            	
            
          
    			return ad;
            		        	
    			            		        	
            }
        
        
	
}

