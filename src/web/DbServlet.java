package web;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadBase.FileSizeLimitExceededException;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.ObjectMapper;

import core.DataClass;
import core.NorthApiCommunicator;
import core.RequestMessageDataTransfer;
import core.ResponseMessageDataTransfer;
import core.UtilityClass;
import model.DbBean;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
 







import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;



/**
 * Servlet implementation class dbServlet
 */

public class DbServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
	
    public DbServlet() {
    	System.out.println("servlet was ran");
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		 String  action  = request.getParameter("action");
		 
		    /*
		     *     Action taken when a manufacturer info is taken. 
		     * 
		     */
		   System.out.println("Action is :" + action);
		   System.out.println("json is :" + request.getParameter("var"));
		   System.out.println("decoded ascii :" + (char)65);
		   
	
		 if(action.equals("getVehicleInfo")){
		 
		
		 final String  var  = request.getParameter("var");

	/*	 DataClass objectData = new DataClass(){
			 
			  String vin = var;
			  
			  public String getVin() {
			  	return vin;
			  }

			  public void setVin(String vin) {
			  	this.vin = vin;
			  }
	 
		 };  */
		 
		 
		 
		 
		  String jsonString =  "{\"protocol\":\"1.4\", \"id\": null, \"session\":\""+(String)request.getSession().getAttribute("sessionKey")+"\",\"type\":\"VehicleInfo\", \"data\":{\"vin\":\""+ var +"\"}, \"seq\": \"1\"}";
		  System.out.println(jsonString);
		 //   1.4   AIS 
	     //  ResponseMessageDataTransfer resDto2 = new NorthApiCommunicator(request.getSession()).sendMessage(new RequestMessageDataTransfer("1.4","null",""+(String)request.getSession().getAttribute("sessionKey")+"","VehicleInfo",objectData,"1"));
		 ResponseMessageDataTransfer resDto2 = new NorthApiCommunicator(request.getSession()).sendMessageUsingJson(jsonString);
		 
		 //   1.6 10.10.16.129     
		 //   ResponseMessageDataTransfer resDto2 = new NorthApiCommunicator(request.getSession()).sendMessageUsingJson("\"protocol\":\"1.6\", \"id\": null, \"session\""+(String)request.getSession().getAttribute("sessionKey")+"\"type\":\"VehicleInfo\", \"data\":{\"vin\":\""+ var +"\"}, \"seq\": 1");
		 
		   
   	   if(!resDto2.getStatus().equals("0")){
	     		   
	     		String message = resDto2.getStatus_str();   
			    response.setContentType("text/html;charset=UTF-8");
		        PrintWriter out = response.getWriter();
		         
		          try {
		              out.println("Error/"+message);
		            
		          } finally {
		              out.close();
		          }
	     		   
	     		   
	     	   }
		 
		 
		 
		  System.out.println("output:  " + ((LinkedHashMap<String,String>) resDto2.getData()).values());    
		  
		  System.out.println("output:  " + ((LinkedHashMap<String,String>) resDto2.getData()).keySet()); 
		  
		  
		  LinkedHashMap<String, String> dataArray = new LinkedHashMap<String, String>();
		  dataArray = (LinkedHashMap<String, String>)resDto2.getData();
		  System.out.println("year:  " + dataArray.get("manu-id"));
		  
		 
		  /*
		   * 
		   *    Get manufacturer of this vehicle 
		   *     
		   * 
		   */
		  
		  
		  /*
		   * 
		   *     Create response and send it jquery.
		   * 
		   * 
		   */
		  
		  
		  response.setContentType("text/html;charset=UTF-8");
          PrintWriter out = response.getWriter();
          
          
          
          try {

        	  ObjectMapper mapper = new ObjectMapper();
        	  String json = mapper.writeValueAsString(dataArray);
        	 // JsonGenerator jqueryJson = null;
        	 // String json = null;
          	// mapper.writeValue(jqueryJson,dataArray);
        	 //  jqueryJson.writeString(json);
              out.println(json);
              

          } finally {
              out.close();
          }
		  
		
	      //System.out.println(request.getParameter("var"));
          
          
          /*
           * 
           *    Action taken when autofill is requested .
           *   
           */
          
          
		   }  else if (action.equals("getAutofil")) {
			  
			       /*
		           * 
		           *    Message 3.13 . Read group info get vehicles .
		           *   
		           */
			   
			   
			   
			    String json =  "{\"protocol\":\"1.6\",\"id\":null,\"session\":\""+(String)request.getSession().getAttribute("sessionKey")+"\",\"type\":\"GroupInfoVins\",\"data\":{\"name\":\"QNX\",\"manu-id\":\"cd307a9a-14e8-4e45-b474-1fc95d476f2d\" ,\"from\":1, \"count\":5},\"seq\":1}";
			   
			    System.out.println(json);
			  
			    
			    NorthApiCommunicator north = new NorthApiCommunicator(request.getSession());
			    
			    
			    ResponseMessageDataTransfer resDtoGroups = north.sendMessageUsingJson(json);
			    
			    LinkedHashMap<String,ArrayList> groupDataArray = (LinkedHashMap<String,ArrayList>)resDtoGroups.getData();
			    
			    
		     	   if(!resDtoGroups.getStatus().equals("0")){
	     	     		   
	     	     		   String message = resDtoGroups.getStatus_str();
	     	     		   
	     	     		
	     	     		   
	     	     	    
     	 			    response.setContentType("text/html;charset=UTF-8");
     	 		        PrintWriter out = response.getWriter();
     	 		          
     	 		          
     	 		          
     	 		          try {

     	 		        
     	 		              out.println("Error/"+message);
     	 		              

     	 		          } finally {
     	 		              out.close();
     	 		          }
	     	     		   
	     	     		   
	     	     	   } else {
			    
			    ArrayList<String> vinList =  groupDataArray.get("vehicles");
			    
			    
	
			       //ObjectMapper mapper = new ObjectMapper();
    	     	  
			    String jsonVinList =  new ObjectMapper().writeValueAsString(vinList);  
    	     			  
			    
			    System.out.println("vehicle vin from group:  " +  vinList.get(3));
			    
			//    String stringVins = "";
			    
			//    for(String s : vinList)  {
			
			//    stringVins = stringVins + s + "#";
			    	
		//	    }
			    
			    response.setContentType("text/html;charset=UTF-8");
		        PrintWriter out = response.getWriter();
		          
		          try {

		              out.println( jsonVinList);   

		          } finally {
		              out.close();
		          }
			    
			    
			    final String  var  = request.getParameter("term");
			    
			    System.out.println("it came from input to autofil :"+var);    
			    
	     	     	   }
	 
		   }   else if (action.equals("addNewVeh")) {
				  
		       /*
	           * 
	           *    Message 3.17 . Add new vehicle .
	           *   
	           */
			   
			
				   final String  json  = request.getParameter("var");
			
				   System.out.println(json);
				  		  
				    ResponseMessageDataTransfer resDtoGroups = new NorthApiCommunicator(request.getSession()).sendMessageUsingJson(json);
				    
				    LinkedHashMap<String,ArrayList> groupDataArray = (LinkedHashMap<String,ArrayList>)resDtoGroups.getData();
				    
				  //  ArrayList<String> vinList =  groupDataArray.get("vehicles"); 
				  //  System.out.println("vehicle vin from group:  " +  vinList.get(3));
		         // removeVeh
	                }  else if (action.equals("deactivateWidget")) {
	                	
	                	
	                	  DbBean db = DbBean.getInstance();
	                	  db.deactivateWidget(request.getParameter("var"));
	                	  //activateWidget
	                	 
	                	  response.setContentType("text/html;charset=UTF-8");
	     	 		      PrintWriter out = response.getWriter();
	     	 		          
	     	 		         
	     	 		          try {

	     	 		        
	     	 		              out.println("Success");
	     	 		              

	     	 		          } finally {
	     	 		        	  
	     	 		              out.close();
	     	 		              
	     	 		              
	     	 		          }
	     	     		   
				    	
	                	
	                	
	                } else if (action.equals("activateWidget")) {
	                	
	                	
	                	 DbBean db = DbBean.getInstance();
	                	 db.activateWidget(request.getParameter("var"));
	                	//activateWidget
	                	 
	                	 
	                	  response.setContentType("text/html;charset=UTF-8");
	     	 		      PrintWriter out = response.getWriter();
	     	 		          
	     	 		         
	     	 		          try {

	     	 		        
	     	 		              out.println("Success");
	     	 		              

	     	 		          } finally {
	     	 		        	  
	     	 		              out.close();
	     	 		              
	     	 		              
	     	 		          }
	     	     		   
				    	
	                	
	                	
	                }  else if (action.equals("refreshWidgets")) {
	                	
	                	  DbBean db = DbBean.getInstance();
	                	  Map<String , String> m  =  db.printWidgetState();
	                	  ObjectMapper mapper = new ObjectMapper();
     	     	     	  
     	     	     	  String versionsString =  mapper.writeValueAsString(m); 
	                	  //activateWidget
	                	  response.setContentType("text/html;charset=UTF-8");
	     	 		      PrintWriter out = response.getWriter();
	     	 		          
	     	 		         
	     	 		          try {

	     	 		        
	     	 		              out.println(versionsString);
	     	 		              

	     	 		          } finally {
	     	 		        	  
	     	 		              out.close();
	     	 		              
	     	 		              
	     	 		          }
	     	     		   
				    	
	                	
	                	
	                }  else if (action.equals("removeVeh")) {
	  				  
	     		       /*
	     	           * 
	     	           *    Message 3.18 . Remove vehicle .
	     	           *   
	     	           */
	     			   
	     				    final String  json  = request.getParameter("var");
	     			
	     				    System.out.println(json);
	     				  		  
	     				    ResponseMessageDataTransfer resDtoGroups = new NorthApiCommunicator(request.getSession()).sendMessageUsingJson(json);
	     				    
	     				    LinkedHashMap<String,ArrayList> groupDataArray = (LinkedHashMap<String,ArrayList>)resDtoGroups.getData();
	     				    
	     				    if( resDtoGroups.getStatus().equals("0")) {
	     				    	
	     		    		//   String message = resDtoGroups.getStatus_str();
	     				    	
	     				    	String vinRemoved = resDtoGroups.getData().toString();
	     	     	     		   
	     				    	String message = "The vehicle number "+vinRemoved+" was successfully removed .";
	     	     	     		   
	     	     	     	    
	     	     	 			    response.setContentType("text/html;charset=UTF-8");
	     	     	 		        PrintWriter out = response.getWriter();
	     	     	 		          
	     	     	 		         
	     	     	 		          try {

	     	     	 		        
	     	     	 		              out.println("Success/"+message);
	     	     	 		              

	     	     	 		          } finally {
	     	     	 		        	  
	     	     	 		              out.close();
	     	     	 		              
	     	     	 		              
	     	     	 		          }
	     	     	     		   
	     				    	
	     				    }  else if (!resDtoGroups.getStatus().equals("0")) {
	     			  		   String message = resDtoGroups.getStatus_str();
	     	     	     		   
	     	     	     		
	     	     	     		   
	     	     	     	    
	     	     	 			    response.setContentType("text/html;charset=UTF-8");
	     	     	 		        PrintWriter out = response.getWriter();
	     	     	 		          
	     	     	 		         
	     	     	 		          try {

	     	     	 		        
	     	     	 		              out.println("Error/"+message);
	     	     	 		              

	     	     	 		          } finally {
	     	     	 		        	  
	     	     	 		              out.close();
	     	     	 		              
	     	     	 		              
	     	     	 		          }
	     	     	     		   
	     				    }
	     				    
	     				  //  ArrayList<String> vinList =  groupDataArray.get("vehicles");
	     				    
	     				  //  System.out.println("vehicle vin from group:  " +  vinList.get(3));
	     		   // removeVeh
	     				    //createNewUF
	     	                } else if (action.equals("createNewUF")) {
	     	                	
	     	     		       /*
	     	     	           * 
	     	     	           *    Message 3.20 . Create new Update File .
	     	     	           *   
	     	     	           */
	     	     			   
	     	     final String  json  = request.getParameter("var");
	     	     			
	     	     System.out.println("UF :"+ json);
	     	     				    
	     	   
	     	     				  		  
	     	     ResponseMessageDataTransfer resDtoGroups = new NorthApiCommunicator(request.getSession()).sendMessageUsingJson(json);
	     	     				    
	     	     LinkedHashMap<String,ArrayList> groupDataArray = (LinkedHashMap<String,ArrayList>)resDtoGroups.getData();
	     	     				    
	     	     				  //  ArrayList<String> vinList =  groupDataArray.get("vehicles");
	     	     				    
	     	     				  //  System.out.println("vehicle vin from group:  " +  vinList.get(3));
	     	     		   // removeVeh
	     	     				    //createNewUF
	     	                              //searchUFButton
	     	     	                }  else if (action.equals("searchUFButton")) {
	    	     	                	
	     	     	     		       /*
	     	     	     	           * 
	     	     	     	           *    Message 3.21 . Get UF info .
	     	     	     	           *   
	     	     	     	           */
	     	     	     			   
	     	     	     final String  json  = request.getParameter("var");
	     	     	     						  		  
	     	     	     ResponseMessageDataTransfer resDtoGroups = new NorthApiCommunicator(request.getSession()).sendMessageUsingJson(json);
	     	     	     				    
	     	     	     LinkedHashMap<String,ArrayList> groupDataArray = (LinkedHashMap<String,ArrayList>)resDtoGroups.getData();
	     	     	     				    
	     	     	     				  // ArrayList<String> vinList =  groupDataArray.get("vehicles");
	     	     	     				    
	     	     	     				  //  System.out.println("vehicle vin from group:  " +  vinList.get(3));
	     	     	     
	     	     	     		   // removeVeh
	     	     	     
	     	     	     				    //createNewUF
	     	     	     
	     	     	                              //searchUFButton
	     	     	     
	     	     	     	                }    else if (action.equals("getUFinfo")) {
	    	    	     	                	
	     	     	     	     		       /*
	     	     	     	     	           * 
	     	     	     	     	           *    Message 3.21 . Get UF info .
	     	     	     	     	           *   
	     	     	     	     	           */
	     	     	     	     			   
	     	     	     	     final String  json  = request.getParameter("var");
	     	     	     	     						  		  
	     	     	     	     ResponseMessageDataTransfer resDtoGroups = new NorthApiCommunicator(request.getSession()).sendMessageUsingJson(json);
	     	     	     	     				    
	     	     	     	     LinkedHashMap<String,ArrayList> groupDataArray = (LinkedHashMap<String,ArrayList>)resDtoGroups.getData();
	     	     	     	     				    
	     	     	     	     //System.out.println(groupDataArray.getClass());	
	     	     	     	     
	     	     	     	     
	     	     	     	     
	     	     	     	     Collection<ArrayList> mapValues = groupDataArray.values();
	     	     	     	     
	     	     	     	     ArrayList<Object> alist = new ArrayList<Object>(mapValues);
	     	     	     	     
	     	     	     	     String jsonResponse = "";
	     	     	     	     
	     	     	     	     System.out.println(alist.toString());
	     	     	     	  
	     	     	     	  String ufID = (String) alist.get(0);  
	     	     	     	
	     	     	     	  String desc = (String) alist.get(1);
	     	     	     	 
	     	     	     	  String name = alist.get(2).toString();
	     	     	     	  
	     	     	     	  String type = (String) alist.get(3);
	     	     	     	     
	     	     	     	  String prov = (String) alist.get(4);
	     	     	     	  
	     	     	     	  ArrayList<String> versions = (ArrayList<String>) alist.get(5);
	     	     	     	  
	     	     	     	  ObjectMapper mapper = new ObjectMapper();
	     	     	     	  
	     	     	     	  String versionsString =  mapper.writeValueAsString(alist.get(5));  
	     	     	     			  
	     	     	        
	     	     	     	  if(versions.isEmpty()){
	     	     	     		  
	     	     	     		  versions.add("null");
	     	     	     	  }
	     	     	     		   
	     	     	    jsonResponse = jsonResponse +  ufID + "#"+ desc + "#" + name + "#"+ type + "#"+ prov+ "#"+ versionsString;
	     	     	    
	     	     	    System.out.println("Type of versions" + alist.get(5));
	     	     	    
	     	     	     		
	     	     	     		  
	     	     	     	
	     	     	     	     	     
	     	     	     	    response.setContentType("text/html;charset=UTF-8");
	     	   		        PrintWriter out = response.getWriter();
	     	   		          
	     	   		          
	     	   		          try {

	     	   		        
	     	   		              out.println(jsonResponse);
	     	   		              

	     	   		          } finally {
	     	   		              out.close();
	     	   		          }
	     	   			    
	     	   			    //getRPinfo
	     	     	     	
	     	     	     	     	                }  else if (action.equals("getRPinfo")) {
	    	    	    	     	                	
	     	     	     	     	     		       /*
	     	     	     	     	     	           * 
	     	     	     	     	     	           *    Message 3.36 . Release package information .
	     	     	     	     	     	           *   
	     	     	     	     	     	           */
	     	     	     	     	     			   
	     	     	     	     	     final String  json  = request.getParameter("var");
	     	     	     	     	     						  		  
	     	     	     	     	     ResponseMessageDataTransfer resDtoGroups = new NorthApiCommunicator(request.getSession()).sendMessageUsingJson(json);
	     	     	     	     	     				    
	     	     	     	     	      
	     	     	     	 	     System.out.println(resDtoGroups.getStatus());
     	     	     	     	     
	     	     	     	     	   if(resDtoGroups.getStatus().equals("1")){
	     	     	     	     		   
	     	     	     	     		   String message = resDtoGroups.getStatus_str();
	     	     	     	     		   
	     	     	     	     		
	     	     	     	     		   
	     	     	     	     	    
	   	     	     	 			    response.setContentType("text/html;charset=UTF-8");
	   	     	     	 		        PrintWriter out = response.getWriter();
	   	     	     	 		          
	   	     	     	 		         
	   	     	     	 		          try {

	   	     	     	 		        
	   	     	     	 		              out.println("Error/"+message);
	   	     	     	 		              

	   	     	     	 		          } finally {
	   	     	     	 		        	  
	   	     	     	 		              out.close();
	   	     	     	 		              
	   	     	     	 		              
	   	     	     	 		          }
	     	     	     	     		   
	     	     	     	     		   
	     	     	     	     	   } else{
	     	     	     	     	     
	     	     	     	     	   
	     	     	     	     	     LinkedHashMap<String,ArrayList> groupDataArray = (LinkedHashMap<String,ArrayList>)resDtoGroups.getData();
	     	     	     	     	     				    
	     	     	     	     	     //System.out.println(groupDataArray.getClass());	
	     	     	     	     	     
	     	     	     	     	    // Collection<ArrayList> mapValues = groupDataArray.values();
	     	     	     	     	     
	     	     	     	     	   
	     	     	     	     	  //   ArrayList<Object> alist = new ArrayList<Object>(mapValues);
	     	     	     	     	     
	     	     	     	     	    ObjectMapper mapper = new ObjectMapper();
	     	   	     	        	      String jsonBack = mapper.writeValueAsString(groupDataArray);
	     	   	     	     	 	       
	     	     	     	     	     
	     	     	     	     	   //  String jsonResponse = "";
	     	     	     	     	     
	     	     	     	     	//     System.out.println(alist.toString());
	     	     	     	     	    
	     	     	     	     	
	     	     	     	     	//  String type = (String) alist.get(1);
	     	     	     	     	 
	     	     	     	     //	 String name = (String) alist.get(2);
	     	     	     	     	 
	     	     	     	     	// Object version = alist.get(3);
	     	     	     	     	     
	     	     	     	     	  //String prov = (String) alist.get(4);
	     	     	     	     	//  String packageId = (String) alist.get(7);
	     	     	     	     	   //  System.out.println(alist.get(7));
	     	     	     	    // 	 String packageId = "test";
	     	     	     	  //  jsonResponse = jsonResponse + type + "#" + name + "#"+ type + "#"+ version+ "#"+ packageId;
	     	     	     	    
	     	     	     	     		System.out.println(jsonBack);	
	     	     	     	     		  
	     	     	     	     	
	     	     	     	   

	     	     	     	     	     
	     	     	     	     	    response.setContentType("text/html;charset=UTF-8");
	     	     	   		        PrintWriter out = response.getWriter();
	     	     	   		          
	     	     	   		          
	     	     	   		          try {

	     	     	   		        
	     	     	   		              out.println(jsonBack);
	     	     	   		              

	     	     	   		          } finally {
	     	     	   		              out.close();
	     	     	   		          }
	     	     	   			    
	     	     	   			 
	     	     	     	     	   }
	     	     	     	     	   }
	     	     	     	                
	     	     	     	                
	     	     	     	                
	     	     	     	                
	     	     	     	                else if (action.equals("getUFAutofil")) {
	    	    	    	     	                	
	     	     	     	     	     		       /*
	     	     	     	     	     	           * 
	     	     	     	     	     	           *    Message 3.35 . Release package information. 
	     	     	     	     	     	           *    Get UF info for the purpose of the autofill.
	     	     	     	     	     	           *   
	     	     	     	     	     	           */
	     	     	     	     	     			   
	     	     	     	     	     // final String  json  = request.getParameter("var");
	     	     	     	                	
	     	     	     	                
	     	     	     	                	/*
	     	     	     	                	 *    DEMO DEMO DEMO
	     	     	     	                	 * 
	     	     	     	                	 */
	     	     	     	     	                	
	     	     	     	     	     String  json   =  "{\"protocol\":\"1.6\", \"id\": null, \"session\":\""+(String)request.getSession().getAttribute("sessionKey")+"\",\"type\":\"RPInfo\",\"data\":{\"uuid\":\"4de32c79-53ef-49f7-bf20-0f3ee199eb60\",\"version\":\"1.0.0\", \"manu-id\":\"cd307a9a-14e8-4e45-b474-1fc95d476f2d\"}, \"seq\": 1}";       	
	     	     	     	     	     						  		  
	     	     	     	     	     ResponseMessageDataTransfer resDtoGroups = new NorthApiCommunicator(request.getSession()).sendMessageUsingJson(json);
	     	     	     	     	     				    
	     	     	     	     	     LinkedHashMap<String, ArrayList> groupDataArray = (LinkedHashMap<String, ArrayList>)resDtoGroups.getData();
	     	     	     	     	     
	     	     	     	     	     System.out.println(resDtoGroups.getStatus());
	     	     	     	     	     
	     	     	     	     	     if(!resDtoGroups.getStatus().equals("0")){
	     	     	     	     		   
	     	     	     	     	     String message = resDtoGroups.getStatus_str();
	     	     	     	     		   
	     	     	     	     	
	   	     	     	 			    response.setContentType("text/html;charset=UTF-8");
	   	     	     	 		        PrintWriter out = response.getWriter();
	   	     	     	 		          
	   	     	     	 		          
	   	     	     	 		          
	   	     	     	 		          try {

	   	     	     	 		        
	   	     	     	 		              out.println("Error/"+message);
	   	     	     	 		              

	   	     	     	 		          } finally {
	   	     	     	 		              out.close();
	   	     	     	 		          }
	     	     	     	     		   
	     	     	     	     		   
	     	     	     	     	   } else {
	     	     	     	     		   
	     	     	     	     		   System.out.println(groupDataArray);
	     	     	     	     	     				    
	     	     	     	     	//System.out.println("UF autofil :  " +  ((LinkedHashMap<String, String>) groupDataArray.get("ufs").get(0)).get("uuid"));
	     	     	     	     	
	     	     	     	     		   
	     	     	     	     		    
	     	     	 			    //    String stringUfs = ((LinkedHashMap<String, String>) groupDataArray.get("ufs").get(0)).get("uuid");
	     	     	 			    
	     	     	 			        String jsonVinList =  new ObjectMapper().writeValueAsString((LinkedHashMap<String, String>) groupDataArray.get("ufs").get(0));  
	     	     	 		//	 LinkedHashMap<String,ArrayList> map = groupDataArray.get("ufs");
	     	     	 			 
	     	     	 			//System.out.println( map.keySet());
	     	     	 			    
	     	     	 		 // for(String s : )  {
	     	     	 			    	
	     	     	 			//	stringUfs = stringUfs + s + " ";
	     	     	 			    	
	     	     	 		 ///  }
	     	     	 			    
	     	     	 			    
	     	     	 			    response.setContentType("text/html;charset=UTF-8");
	     	     	 		        PrintWriter out = response.getWriter();
	     	     	 		          
	     	     	 		          
	     	     	 		          
	     	     	 		          try {

	     	     	 		        
	     	     	 		              out.println( jsonVinList);
	     	     	 		              

	     	     	 		          } finally {
	     	     	 		              out.close();
	     	     	 		          }
	     	     	     	     	   }
	     	     	     	     	     
	     	     	     	     	     	                }  else if (action.equals("logOut")){
		   
	     	     	     	     	     	              
	     	     	     	     	     	               request.getSession().invalidate(); 
	     	     	     	     	     	         
	     	     	     	     	     	            
	     	     	     	     	     	          
	     	     	     	     	     	                }   else if (action.equals("setSessionKey")) {
	     	     	     	                	
	     	     	     	                	
	     	     	     	                		  DbBean db = DbBean.getInstance();
	     	     	     	                		  
	     	     	     	                		  
	     	     	     	                	      db.setKey( request.getParameter("var"));      
	     	     	     	                	
	     	     	     	                	      
	     	     	     	  	
	     	     	     	                }    else if (action.equals("getSessionKey")) {
	     	     	     	                	
	     	     	     	                	
   	     	     	                		  DbBean db = DbBean.getInstance();
   	     	     	                		  
   	     	     	                		  
   	     	     	                	     String key =  db.getKey();  
   	     	     	                	     

   		     	     	     	     	    response.setContentType("text/html;charset=UTF-8");
   		     	     	     	     	    
   		     	     	     	     	    
   		     	     	   		            PrintWriter out = response.getWriter();
   		     	     	   		          
   		     	     	   		          
   		     	     	   		          try {

   		     	     	   		        
   		     	     	   		              out.println(key);
   		     	     	   		              

   		     	     	   		          } finally {
   		     	     	   		              out.close();
   		     	     	   		          }
   	     	     	                	
   	     	     	                	  
   	     	     	  	
   	     	     	                        }   else if (action.equals("setVBSAddress")) {
	     	     	     	                	
	     	     	     	                	
   	     	     	                		   DbBean db = DbBean.getInstance();
   	     	     	                		   
   	     	     	                		   System.out.println(request.getParameter("var"));
   	     	     	                		  
   	     	     	                	       db.setAddrrInDb( request.getParameter("var"));   
   	     	     	                	       
   	     	     	                	       
   	     	     	                	
   	     	     	                           } else if (action.equals("getVbsAddress")) {
   	   	     	     	                       //getSessionKeyFromJSP 
 	     	     	     	                 try{
    	     	     	               
    	     	     	                	     
    	     	     	                
 	     	     	     	                	HttpSession session = request.getSession();
    	     	     	                 
    	     	     	                 if(session.getAttribute("vbsAddress") == null)  {
    	     	     	                	 
    	     	     	                	 
    	     	     	  
    	     	     	                	 
    	     	     	                	  DbBean db = DbBean.getInstance();
	     	     	                		   
    	    	     	     	                 String ip =  db.getIpAddrr(); 
    	    	     	     	                	           
    	    	     	     	                 System.out.println("ip is"+ip);
    	    	     	     	                 
    	    	     	     	                 
    	    	     	     	               
    	 
    	     	     	                 session.setAttribute("vbsAddress", ip);
    	     	     	                 
    	     	     	                 
    	     	     	                 }
    	     	     	                  	     
 	     	     	     	                 } catch (NullPointerException e){
 	     	     	     	                  	 
 	     	     	     	                  System.out.println("No address available as db was not started");	 
 	     	     	     	                	 
 	     	     	     	                 }
    	     	     	                 
    	     	     	                	//  RequestDispatcher rd = getServletContext().getRequestDispatcher("/portal.jsp");
    	     	     	                    //  rd.forward(request, response);
    	     	     	                	     
    	     	     	                	//    SettingsBean sb = new SettingsBean();  
    	     	     	                	 //   sb.setSessionKey(key);
    	     	     	  
    		     	     	     	     	   
    	     	     	                	     //getSessionKeyFromJSP 
    	     	     	  	
    	     	     	                        }    
	     	     	     	                
	     	     	     	                
	     	     	     	                else if (action.equals("getSessionKeyFromJSP")) {
   	     	     	                       //getSessionKeyFromJSP 
	     	     	     	                 
   	     	     	                	     
   	     	     	                	  
   	     	     	                		  HttpSession session = request.getSession();
   	     	     	                		  if(session.getAttribute("sessionKey") == null){
   	     	     	                			  
   	     	     	                			  
   	     	     	                			  
   	     	     	                		  System.out.println("getting key");
   	     	     	                		 
   	     	     	                		 
  	     	     	                		  DbBean db = DbBean.getInstance();
  	     	     	                		  
  	     	     	                	       String key = "";
  	     	     	                		  
  	     	     	                		  try{
  	     	     	                			  
  	     	     	                	      key =  db.getKey();  
  	     	     	                	      
  	     	     	                		  } catch (NullPointerException e){
  	     	     	                			  
  	     	     	                			System.out.println("No key, database probably not started");
  	     	     	                			
  	     	     	                		  }
   	     	     	                		  
   	     	     	                 
   	     	     	                           session.setAttribute("sessionKey", key);
   	     	     	                	     
   	     	     	                		  }
   	     	     	                	//  RequestDispatcher rd = getServletContext().getRequestDispatcher("/portal.jsp");
   	     	     	                    //  rd.forward(request, response);
   	     	     	                	     
   	     	     	                	//    SettingsBean sb = new SettingsBean();  
   	     	     	                	 //   sb.setSessionKey(key);
   	     	     	  
   		     	     	     	     	   
   	     	     	                	     //getSessionKeyFromJSP 
   	     	     	  	                     //addNewPackage
   	     	     	                        }  else if (action.equals("addNewPackage")) {
   	     	     	                        	     	
   	     	     	                 final String  json  = request.getParameter("var");
   	     		     	     			
   	     	     	 	     	     System.out.println("New Package :"+ json);
   	     	     	 	     	     				    		  		  
   	     	     	 	     	     ResponseMessageDataTransfer resDtoGroups = new NorthApiCommunicator(request.getSession()).sendMessageUsingJson(json);
   	     	     	 	     	     				    
   	     	     	                           // addNewVersionToUfFIle  
   	     	     	 	     	     
   	   	     	     	 	  	   if(!resDtoGroups.getStatus().equals("0")){
   	   	     	     	 	  		   
   	   	     	     	 	  	
   	 	     	     	     		    String message = resDtoGroups.getStatus_str();
   	 	     	     	     		   
   	 	     	     	 
   		     	     	 			    response.setContentType("text/html;charset=UTF-8");
   		     	     	 		        PrintWriter out = response.getWriter();
   		     	     	 		          
   		     	     	 		         
   		     	     	 		          try {

   		     	     	 		              out.println("Error/"+message);
   		     	     	 		              
   		     	     	 		          } finally {
   		     	     	 		              out.close();
   		     	     	 		          }
   	     	     	 	     	     
   	   	     	     	 	  	   } else {
   	   	     	     	 	  		   
   	   	     	     	 	  		   
   	   	     	     	 	       LinkedHashMap<String,ArrayList>groupDataArray = (LinkedHashMap<String,ArrayList>)resDtoGroups.getData();
   	   	     	     	 	      ObjectMapper mapper = new ObjectMapper();
   	   	     	        	      String jsonBack = mapper.writeValueAsString(groupDataArray);
   	   	     	     	 	       
   	   	     	        	      
   	   	     	            response.setContentType("text/html;charset=UTF-8");
   	     	 		        PrintWriter out = response.getWriter();
   	     	 		          
   	   	     	        	      
   	   	     	        	  try {

    	     	 		              out.println(jsonBack);
    	     	 		              
    	     	 		          } finally {
    	     	 		              out.close();
    	     	 		          }
   	   	     	        	          
   	   	     	     	 	  		   
   	   	     	     	 	  	   }
   	     	     	 	     	     
   	     	     	                 }  else if (action.equals("addNewVersionToUfFIle")) {
   	     	     	                        	
   	     	     	                 final String  json  = request.getParameter("var");
   	     		     	     			
   	     	     	 	     	     System.out.println("New Version to update file :" + json);
   	     	     	 	     	     				    
   	     	     	 	 	  		  
   	     	     	 	     	     ResponseMessageDataTransfer resDtoGroups = new NorthApiCommunicator(request.getSession()).sendMessageUsingJson(json);
   	     	     	 	     	     				    
   	     	     	 	     	     LinkedHashMap<String,ArrayList> groupDataArray = (LinkedHashMap<String,ArrayList>)resDtoGroups.getData();
   	     	     	                        	
   	     	     	                           // addNewVersionToUfFIle    
   	     	     	 	     	     
   	     	     	 	     	     
   	     	     	 	  	   if(!resDtoGroups.getStatus().equals("0")){
   	     	     	 	  		   
   	     	     	 	  		   
 	     	     	     		   
 	     	     	     		    String message = resDtoGroups.getStatus_str();
 	     	     	     		   
 	     	     	     		
 	     	     	     		
 	     	     	     		   
 	     	     	     	    
	     	     	 			    response.setContentType("text/html;charset=UTF-8");
	     	     	 		        PrintWriter out = response.getWriter();
	     	     	 		          
	     	     	 		          
	     	     	 		          
	     	     	 		          try {

	     	     	 		              out.println("Error/"+message);
	     	     	 		              
	     	     	 		          } finally {
	     	     	 		              out.close();
	     	     	 		          }
 	     	     	     		   
 	     	     	     		   
 	     	     	     	   } else {
 	     	     	     		   
 	     	     	     		
 	     	     	     		   
 	     	     	     		   try{
 	     	     	     		   
 	     	     	     		 //  System.out.println(groupDataArray.get("ftpURL").get(3));
 	     	     	     		   
 	     	     	            //ftp = UtilityClass.decodeAsciCodes(groupDataArray.get("ftpURL"));
 	     	     	     			   
 	     	     	     			   
 	     	     	     		    
 	     	     	     		      
 	     	     	     		   System.out.println(groupDataArray.get("ftpURL"));
 	     	     	     		   
 	     	     	     		   	      				    
 	     	     	     	//System.out.println("UF autofil :  " +  ((LinkedHashMap<String, String>) groupDataArray.get("ufs").get(0)).get("uuid"));
 	     	     	     	
 	     	 			    //  String stringUfs = ((LinkedHashMap<String, String>) groupDataArray.get("ufs").get(0)).get("uuid");
 	     	 			    
 	     	 	        	//	 LinkedHashMap<String,ArrayList> map = groupDataArray.get("ufs");
 	     	 			 
 	     	 		     	//System.out.println( map.keySet());
 	     	 			    
 	     	 		        // for(String s : )  {
 	     	 			    	
 	     	 		    	//	stringUfs = stringUfs + s + " ";
 	     	 			    	
 	     	 		        ///  }
 	     	 			    
 	     	 			    
 	     	     	     	   } catch(NullPointerException  |  ClassCastException c) {
	     	     	     			   
	     	     	     			   c.printStackTrace();
	     	     	     			   
	     	     	     		   }
 	     	     	     		   	   
 	     	 			    response.setContentType("text/html;charset=UTF-8");
 	     	 			    
 	     	 		        PrintWriter out = response.getWriter();
 	     	 		          
 	     	 		          try {

 	     	 		             out.println((String)UtilityClass.decodeAsciCodes(groupDataArray.get("ftpURL")));
 	     	 		              
 	     	 		          } finally {
 	     	 		        	  
 	     	 		              out.close();
 	     	 		                 
 	     	 		          }
 	     	 		          
 	     	     	     	   }
   	     	     	 	     	     
   	     	     	                   }  else if (action.equals("makeUpdateOnAFile")){
   	     	     	                        	
   	     	     	                        	
   	     	     	                    
   	     	     	               	 	JSch jsch = new JSch();
   	     	     	               	      
   	     	     	               		
   	     	     	               		String username = "root"; 
   	     	     	                   //    String host = "204.68.122.215/mnt/glusterfs/carsync/package-upload";
   	     	     	               		String host = "204.68.122.215";
   	     	     	               		
   	     	     	               		
   	     	     	               		int port = 22; 	
   	     	     	                        	
   	     	     	               Session session2; 	
   	     	     	                  
									try {
										session2 = jsch.getSession(username, host, port);
										session2.setPassword("$CarSync001$");
										session2.setConfig("StrictHostKeyChecking", "no");
										System.out.println("Establishing Connection...");
										session2.connect();
										System.out.println("Connection established.");
										System.out.println("Creating SFTP Channel. for Curl");
										
										System.out.println("Triggering the updated from VBS to Tizen device");
										ChannelExec channelExec = (ChannelExec)session2.openChannel("exec");
										//channelExec.setCommand("ping 204.68.122.215:8080");
										
										channelExec.connect();
	   	     	     	                channelExec.disconnect();
	   	     	     	                
	   	     	     	                NorthApiCommunicator northApiCom = new  NorthApiCommunicator(request.getSession());
	   	     	     	                northApiCom.sendMessageUsingJson("");
	   	     	     	                
	   	     	     	           String jsonString =  "{\"protocol\":\"1.4\", \"id\": null, \"session\": null, \"type\":\"Ping\", \"data\": null, \"seq\": 1}";
	   	     	     	           System.out.println("we are sending PING to trigger the update");
	   	     	     		       System.out.println(jsonString);
	   	     	     		       //   1.4   AIS 
	   	     	     	           //  ResponseMessageDataTransfer resDto2 = new NorthApiCommunicator(request.getSession()).sendMessage(new RequestMessageDataTransfer("1.4","null",""+(String)request.getSession().getAttribute("sessionKey")+"","VehicleInfo",objectData,"1"));
	   	     	     	            ResponseMessageDataTransfer resDto2 = new NorthApiCommunicator(request.getSession()).sendMessageUsingJson(jsonString);
	   	     	     	                
	   	     	     	                
	   	     	     	                
	   	     	     	                  // ChannelSftp sftpChannel = (ChannelSftp) session2.openChannel("sftp");
	   	     	     	                 
	   	     	     	                 // sftpChannel.setCwd("cd /root/vbs_git/arynga.carsync.vbs/priv");
	   	     	     	                 // sftpChannel.connect();
	   	     	     	                 // sftpChannel.cd("/root/vbs_git/arynga.carsync.vbs/test");
	   	     	     	               
	   	     	     	                  
	   	     	     	                  System.out.println("Update triggered from VBS to a Tizen device.");
	   	     	     	                 // File f1 = new File("C:/Users/T430/Desktop/KNOWLEDGE_BASE/MadisonMockup.pdf");
	   	     	     	                 // channel.disconnect();
	   	     	     	                 // Channel channel = session1.openChannel("exec");
	   	     	     	                 // ((ChannelExec) channel).setCommand("cd /root/vbs_git/arynga.carsync.vbs/priv");

	   	     	     	                 // channel.setInputStream(null);
	   	     	     	                 // channel.setOutputStream(null);
	   	     	     	                 // channel.connect();
	   	     	     	                
	   	     	 
									} catch (JSchException e) {
										System.out.println("jsch exception or sftp exception");
										e.printStackTrace();
									}
   	     	     	                 
   	     	     	                        	
   	     	     	                        	
   	     	     	                        	
   	     	     	                        }
		   
		                                            
	                            }
	
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		
		
		
		
		
		
		
		         
		
		
		

		 String  action  = request.getParameter("action");
		 
		    /*
		     * 
		     *     Action taken when a manufacturer info is taken. 
		     * 
		     * 
		     */
		   System.out.println("Action is :" + action);
		   
		   if(action != null){
		   
		    if(action.equals("fileUpload")) {
			   
			  
		
	        	
			//  SampleFileUpload fileUpload = new SampleFileUpload () ;
		    //  File file = new File ("C:/Users/T430/Documents/BoatConnect.pdf") ;
		 
		   //   String response1 = fileUpload.executeMultiPartRequest("http://204.68.122.215/package-upload", file, file.getName(), "File Upload test Hydrangeas.jpg description") ;
		   //   System.out.println("Response : "+response1);
			   
			  
			//  HttpClient client = new DefaultHttpClient();
			//  HttpPost post = new HttpPost("http://204.68.122.215/package-upload/2df21282-f8c8-4d2d-a5c4-249a064d55f3-vfvfvfvfv/");  
			   
			   
			//   MultipartEntity entity = new MultipartEntity();
			   
			//   entity.addPart("file", new FileBody(file));
			//   post.setEntity(entity);

			//   HttpResponse response = httpclient.execute(post);
			   
		//System.out.println(request.getParameter("fileName"));
			   
		    	
		    	
		    	
				   
	 if (ServletFileUpload.isMultipartContent(request)) {
		        
		 
		 
		 sftpConnectionFileUpload( handleMultiPartContent(request));
		 
		 
		//  handleMultiPartContent(request);
		        }
			   
			   
			   
			      response.setContentType("application/json;charset=UTF-8");
	 		       PrintWriter out = response.getWriter();
	 		          
	 		          
	 		          
	 		         try {
	 		        	  
	 		        	  
	 		          String json = "{\"message\": \"file was uploaded\"}";

 out.println(json);
	 		              
	 		          } finally {
	 		              out.close();
	 		         }
	 		         
	 		         
	     		      	   
		   } }else {
		
		
	DbBean db = DbBean.getInstance();
	
	
	boolean passed = false;
	String errorMessage = "";
	
	try{
	
	passed = db.verifyCredentials(request.getParameter("userInput").trim(), request.getParameter("passwordInput").trim(), request);
	
	
	} catch (NullPointerException e) {
		
		System.out.println("Cannot check credentials");
	    errorMessage = "one";
			
	}
	
	
	if(passed){
		
		// HttpSession session = request.getSession(true);
		 
		 response.sendRedirect("portal.jsp");
		
	} else {
		
		 if(!errorMessage.equals("one")){
		 errorMessage = "two";
		 }
		/// response.sendRedirect("login.jsp?passed=no&error="+errorMessage);
		 request.setAttribute("error",errorMessage);
		 request.setAttribute("passed","no");
		 RequestDispatcher RD = getServletContext().getRequestDispatcher("/login.jsp");
		 RD.forward(request, response);	
	}
	
		   }
		   
	}
		   	   
	
		
	    public void sftpConnectionFileUpload(File file)  {
		
		
	 	JSch jsch = new JSch();
	      
		
		String username = "root"; 
    //    String host = "204.68.122.215/mnt/glusterfs/carsync/package-upload";
		String host = "204.68.122.215";
		
		
		int port = 22;
		
		
		
		try {
			
	    Session session1 = jsch.getSession(username, host, port);
        session1.setPassword("$CarSync001$");
        session1.setConfig("StrictHostKeyChecking", "no");
        System.out.println("Establishing Connection...");
        session1.connect();
        System.out.println("Connection established.");
        System.out.println("Crating SFTP Channel.");
        
        
        
        ChannelExec channelExec = (ChannelExec)session1.openChannel("exec");

        
        channelExec.setCommand("rm -rf /mnt/glusterfs/carsync/packages/366a924f-5ca5-4f3f-8b9e-95cb7da313c2/6.6.6; cd /root/vbs_git/arynga.carsync.vbs/priv ; ./vbspkg.py prepare-temp 366a924f-5ca5-4f3f-8b9e-95cb7da313c2 6.6.6");
        channelExec.connect();
        
        
        //channelExec.setCommand("cd /root/vbs_git/arynga.carsync.vbs/priv && ./vbspkg.py prepare-package 366a924f-5ca5-4f3f-8b9e-95cb7da313c2 1 ");
        //channelExec.connect();
        //channelExec.disconnect();
        
        ChannelSftp sftpChannel = (ChannelSftp) session1.openChannel("sftp");
       
        // sftpChannel.setCwd("cd /root/vbs_git/arynga.carsync.vbs/priv");

        try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        sftpChannel.connect();
        sftpChannel.cd("/mnt/glusterfs/carsync/package-upload/366a924f-5ca5-4f3f-8b9e-95cb7da313c2-6.6.6");
     
        
        System.out.println("SFTP Channel created.");
      //  File f1 = new File("C:/Users/T430/Desktop/KNOWLEDGE_BASE/MadisonMockup.pdf");
        
       
       // channel.disconnect();
       // Channel channel = session1.openChannel("exec");
      //  ((ChannelExec) channel).setCommand("cd /root/vbs_git/arynga.carsync.vbs/priv");

      //  channel.setInputStream(null);
     //   channel.setOutputStream(null);
        
        
     //   channel.connect();
      
        try {
			sftpChannel.put(new FileInputStream(file) , file.getName(), ChannelSftp.OVERWRITE);
		} catch (FileNotFoundException e) {
			 System.out.println("sftp file upload exception");
			e.printStackTrace();
		}
		  

        ChannelExec channelExec2 = (ChannelExec)session1.openChannel("exec");

        //rm -rf /mnt/glusterfs/carsync/packages/366a924f-5ca5-4f3f-8b9e-95cb7da313c2/1.1.1;
        channelExec2.setCommand("cd /root/vbs_git/arynga.carsync.vbs/priv ; cp /root/tizen-scripts.tar /mnt/glusterfs/carsync/package-upload/366a924f-5ca5-4f3f-8b9e-95cb7da313c2-1.1.1 ; ./vbspkg.py prepare-package 366a924f-5ca5-4f3f-8b9e-95cb7da313c2 1.1.1");
        channelExec2.connect();
        
        
    	} catch (JSchException e) {
    		
    		 System.out.println("JSchException");
        
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SftpException e) {
			 System.out.println("SftpException");
			e.printStackTrace();
		}
        	
		
			
	  }
	
	  private File handleMultiPartContent(HttpServletRequest request) {

		  File tempFile = null;
		  
	        ServletFileUpload upload = new ServletFileUpload();
	        upload.setFileSizeMax(600000000); // 
	        try {
	            FileItemIterator iter = upload.getItemIterator(request);
	            while (iter.hasNext()) {
	                FileItemStream item = iter.next();
	                if (!item.isFormField()) {
	                   tempFile = saveFile(item,request.getParameter("fileName"));
	                   // UtilityClass.ftpDispacher(tempFile);
	                 
	                    
	                    /*
	                     * 
	                     *      File dispatch block
	                     * 
	                     * 
	                     */
	                   
	                    // process the file
	        			
	        			
	        			// System.out.println("command linux :" + Runtime.getRuntime().exec("pwd"));
	                }
	            }
	        }
	        catch (FileUploadException e) {
	            System.out.println("Error uploading file");
	        }
	        catch (IOException e) {
	        	System.out.println("Error uploading file");
	        }
	        
	        
	        return tempFile;
	    }

	  
	    private File saveFile(FileItemStream item , String name) {

	    	
	        InputStream in = null;
	        OutputStream out = null;
	        try {
	            in = item.openStream();
	              File tmpFile = File.createTempFile("./updateFiles", null);
	              File file = new File(name);
	        //  UtilityClass.ftpDispacher();
	            
	          //  tmpFile.deleteOnExit();
	            out = new FileOutputStream(file);
	            long bytes = 0;
	            byte[] buf = new byte[1024];
	            int len;
	            while ((len = in.read(buf)) > 0) {
	                out.write(buf, 0, len);
	                bytes += len;
	            }
	            System.out.println(String.format("Saved %s bytes to %s ", bytes, tmpFile.getCanonicalPath()));
	            return file;
	        }
	        catch (IOException e) {

	        	System.out.println("Could not save file");
	            Throwable cause = e.getCause();
	            if (cause instanceof FileSizeLimitExceededException) {
	            	System.out.println("File too large");
	            }
	            else {
	            	System.out.println("Technical error");
	            }
	            return null;
	        }
	        finally {
	            try {
	                if (in != null) {
	                    in.close();
	                }
	                if (out != null) {
	                    out.close();
	                }
	            }
	            catch (IOException e) {
	            	System.out.println("Could not close stream");
	            }
	        }
	    }
	    
	   
	    public class SampleFileUpload {
	    	 
	        /**
	         * A generic method to execute any type of Http Request and constructs a response object
	         * @param requestBase the request that needs to be exeuted
	         * @return server response as <code>String</code>
	         */
	    	
	        private String executeRequest(HttpRequestBase requestBase){
	            String responseString = "" ;
	     
	            InputStream responseStream = null ;
	            HttpClient client = new DefaultHttpClient () ;
	            try{
	                HttpResponse response = client.execute(requestBase) ;
	                if (response != null){
	                    HttpEntity responseEntity = response.getEntity() ;
	     
	                    if (responseEntity != null){
	                        responseStream = responseEntity.getContent() ;
	                        if (responseStream != null){
	                            BufferedReader br = new BufferedReader (new InputStreamReader (responseStream)) ;
	                            String responseLine = br.readLine() ;
	                            String tempResponseString = "" ;
	                            while (responseLine != null){
	                                tempResponseString = tempResponseString + responseLine + System.getProperty("line.separator") ;
	                                responseLine = br.readLine() ;
	                            }
	                            br.close() ;
	                            if (tempResponseString.length() > 0){
	                                responseString = tempResponseString ;
	                            }
	                        }
	                    }
	                }
	            } catch (UnsupportedEncodingException e) {
	                e.printStackTrace();
	            } catch (ClientProtocolException e) {
	                e.printStackTrace();
	            } catch (IllegalStateException e) {
	                e.printStackTrace();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }finally{
	                if (responseStream != null){
	                    try {
	                        responseStream.close() ;
	                    } catch (IOException e) {
	                        e.printStackTrace();
	                    }
	                }
	            }
	            client.getConnectionManager().shutdown() ;
	     
	            return responseString ;
	        }
	     
	        /**
	         * Method that builds the multi-part form data request
	         * @param urlString the urlString to which the file needs to be uploaded
	         * @param file the actual file instance that needs to be uploaded
	         * @param fileName name of the file, just to show how to add the usual form parameters
	         * @param fileDescription some description for the file, just to show how to add the usual form parameters
	         * @return server response as <code>String</code>
	         */
	        @SuppressWarnings("deprecation")
			public String executeMultiPartRequest(String urlString, File file, String fileName, String fileDescription) {
	     
	            HttpPost postRequest = new HttpPost (urlString) ;
	            try{
	     
	                @SuppressWarnings("deprecation")
					MultipartEntity multiPartEntity = new MultipartEntity () ;
	     
	                //The usual form parameters can be added this way
	                multiPartEntity.addPart("fileDescription", new StringBody(fileDescription != null ? fileDescription : "")) ;
	                multiPartEntity.addPart("fileName", new StringBody(fileName != null ? fileName : file.getName())) ;
	     
	                /*Need to construct a FileBody with the file that needs to be attached and specify the mime type of the file. Add the fileBody to the request as an another part.
	                This part will be considered as file part and the rest of them as usual form-data parts*/
	                FileBody fileBody = new FileBody(file, "application/octect-stream") ;
	                multiPartEntity.addPart("attachment", fileBody) ;
	     
	                postRequest.setEntity(multiPartEntity) ;
	            }catch (UnsupportedEncodingException ex){
	                ex.printStackTrace() ;
	            }
	     
	            return executeRequest (postRequest) ;
	        }  
	
	
	    }
}
